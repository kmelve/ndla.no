<?php
// $Id$
/**
 * @file
 * @ingroup contentbrowser
 */
?>
  <script type='text/javascript'>
    //Rather ugly but (document).ready() doesnt work with AHAH. Instead we call this function after
    //X milliseconds.
    timeout_id = 0;
    function performClick(element_id, contentbrowser_id) {
      //Clear the timeout.
      $("#" + element_id).click();
    }
  </script>
<?php
$instance = $_REQUEST['instanceId'];
?>
<div id='contentbrowser_search_results'>
  <?php
  $types = node_get_types();
  $language_list = language_list();
  $onclick = "";
  $as_output = FALSE;
  if(!empty($_REQUEST['output']) && $_REQUEST['output'] != 'default') {
    $as_output = TRUE;
    $onclick = " onclick=\"contentbrowser_return(this); return false;\" ";
    if($_REQUEST['output'] == 'json') {
      $onclick .= "class='json-output' ";
    } else if($_REQUEST['output'] == 'nid') {
      $onclick .= "class='nid-output' ";
    } else if($_REQUEST['output'] == 'path') {
      $onclick .= "class='path-output' ";
    }
  }
  global $base_url;
  if(is_array($results) && count($results)) {
    $searched_nid = (isset($_REQUEST['searched_nid']) && is_numeric($_REQUEST['searched_nid'])) ? check_plain($_REQUEST['searched_nid']) : NULL;
    $contentbrowser_id = isset($_REQUEST['contentbrowser_id']) ? check_plain($_REQUEST['contentbrowser_id']) : NULL;
    if(!$thumbail_list) {
      print "<table><tr><th>".t('Title')."</th><th>".t('Nodetype')."</th><th>" . t('Language') . "</th></tr>";
    }
    foreach($results as $media) {
      $modal = (!$as_output) ? "lightmodal[|height:600px; width: 800px; scrolling:auto;]" : "";
      $open_link = "<a $onclick id='node_".$media['nid'] . "' title='" . $media['title'] . "' rel='$modal' href='".$base_url . "/contentbrowser/details/" . $media['nid'] . "?instanceId=" . $instance;
      $open_link .= (($searched_nid == $media['nid'] && $contentbrowser_id) ? "&tag=".urlencode($contentbrowser_id) : "") . "'>";
      if($thumbail_list) {
        print "<div class='contentbrowser_thumbnail'>";
        print "<p>". (!empty($types[$media['type']]->name) ? $types[$media['type']]->name : $media['type'])."</p>";
        print $open_link;
        /**
         * @todo Delete this when istribute supports imagecache
         */
        $got_istr = FALSE;
        if($media['drupal_type'] == 'video') {
          $n = node_load($media['nid']);
          if(istribute_has_video($n)) {
            print "<img src='" . istribute_get_thumbnail($n, 200, 200) . "' width='100' />";
            $got_istr = TRUE;
          }
        }
        if(!$got_istr) {
          print "<img src='" . $media['thumbnail'] . "' />";
        }
        print "</a>";
        print "<p>" . $media['title'] . "</p>";
        print "</div>";
      }
      else {
        print "<tr><td>$open_link".$media['title']."</a></td>";
        print "<td>".$media['type']."</td>";
        $lang = $language_list[$media['language']]->native;
        if(empty($lang)) {
          $lang = t('Neutral');
        }
        print "<td>".$lang."</td></tr>";
      }
      
      if($media['nid'] == $searched_nid) {
        print "<script type='text/javascript'>";
        print "timeout_id = setTimeout('performClick(\"node_" . $media['nid'] . "\")', 1000);";
        print "</script>";
      }
    }
    if(!$thumbnail_list) {
      print "</table>";
    }
  }
  else {
    print t('Nothing found') . " :(";
  }
  ?>
</div>
<div class='contentbrowser_pager'>
  <?php
    $pager = str_replace("/get_results", "", theme('pager'));
    print $pager;
  ?>
</div>



