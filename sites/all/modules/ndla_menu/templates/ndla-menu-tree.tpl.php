<li class="<?php print $menu_item->li_classes ?>" role="treeitem" aria-labelledby="title-<?php print $aria_id ?>"<?php ($aria_level != NULL) ? print ' aria-level="' . $aria_level . '"' : NULL ?>>
  <div class="<?php print $menu_item->classes ?>">
    <?php if ($menu_item->children || $menu_item->has_content): ?>
      <a href="javascript:void(0);" class="arrow" aria-expanded="<?php print $menu_item->expanded ? 'true' : 'false' ?>" aria-controls="sub-menu-<?php print $aria_id ?>" title="<?php print t('expand menu item @title', array('@title' => $menu_item->title)) ?>"></a>
    <?php else: ?>
      <span class="arrow"></span>
    <?php endif ?>
    <div class="name" id="title-<?php print $aria_id ?>"><?php print $link ? $link : check_plain($menu_item->title) ?></div>
  </div>
  <?php if ($menu_item->children != ''): ?>
    <ul id="sub-menu-<?php print $aria_id ?>" role="group" aria-hidden="<?php print $menu_item->expanded ? 'false' : 'true' ?>">
      <?php print $menu_item->children ?>
    </ul>
  <?php endif ?>
</li>