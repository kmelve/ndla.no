<?php
/**
 * @file
 * @ingroup ndla_content_sync
 */
 
/**
 * @file
 * Contains our implementations of content_sync node hooks:
 *  - hook_content_sync_node_alter: alter a node before it is saved.
 */

/**
 * Implement hook_content_sync_node_alter on behalf of path module.
 */
function path_content_sync_node_alter(&$node, $node_exists_locally) {
  unset($node->path);
}

/**
 * Implement hook_content_sync_node_alter on behalf of comment module.
 */
function comment_content_sync_node_alter(&$node, $node_exists_locally) {
  unset($node->comment_count);
  unset($node->last_comment_timestamp);
  unset($node->last_comment_name);
}

/**
 * Implement hook_content_sync_node_alter on behalf of nodequeue module.
 */
function nodequeue_content_sync_node_alter(&$node, $node_exists_locally) {
  unset($node->queues);
}

/**
 * Implement hook_content_sync_node_alter on behalf of book module.
 */
function book_content_sync_node_alter(&$node, $node_exists_locally) {
  if (!$node_exists_locally) {
    // Unset menu ID, to trigger creation of new book entry
    unset($node->book['mlid']);
  }
  else {
    // Since there does not seem to be any way to keep remote and local menu ID's in sync,
    // we have to replace the remote ID with the local one stored in the book table, as it
    // seems this never changes, even when book pages are moved to different books
    $node->book['mlid'] = db_result(db_query("SELECT mlid FROM {book} WHERE nid=%d", $node->book['nid']));
  }
}

/**
 * Implement hook_content_sync_node_alter on behalf of taxonomy module.
 */
function taxonomy_content_sync_node_alter(&$node, $node_exists_locally) {
  // Rewrites the taxonomy object to look like expected for the taxonomy module
  if ($node->taxonomy) {
    // construct array of term names from the $node->taxonomy
    $terms = array();
    foreach ($node->taxonomy as $term) {
      if (isset($term)) {
        if (isset($term->tid)) {
          $terms[] = $term->tid;
        }
        else {
          // Terms in vocabularies that are handled by the content_taxonomy module
          // are restructured by the modules presave. Handle those here.
          foreach ($term as $value) {
            $terms[] = $value;
          }
        }
      }
    }
    // parse the taxonomy and return taxonomy array to be attached to the node object
    $node->taxonomy = $terms;
  }
}

/**
 * Implement hook_content_sync_node_alter on behalf of location module.
 */
function location_content_sync_node_alter(&$node, $node_exists_locally) {
  if ($node->locations) {
    foreach ($node->locations as &$loc) {
      _ndla_content_sync_write_record('location', $loc, 'lid');
    }
  }
}

function utdanning_rdf_content_sync_node_alter(&$node, $node_exists_locally) {
  if ($node->utdanning_rdf) {
    // Re-map the vid in all relations, since all nodes get new vids on this site
    foreach ($node->utdanning_rdf as $rel) {
      $rel[0] = "vid:{$node->vid}";
    }
  }
}

function image_content_sync_node_alter(&$node, $node_exists_locally) {
  if ($node->type == 'image') {
    $original_image = $node->images[IMAGE_ORIGINAL];
    if ($node_exists_locally) {
      $current_file = db_result(db_query("SELECT f.filepath FROM {image} i INNER JOIN {files} f ON i.fid = f.fid WHERE i.nid = %d AND i.image_size = '%s'", $node->nid, IMAGE_ORIGINAL));
      if ($current_file != $original_image) {
        // A new image has been uploaded. Notify the image module
        error_log("Image updated for node " . $node->nid);
        $node->new_file = TRUE;
      }
    }
    if (!file_exists($original_image)) {
      $node->images = array();
      $translate_replacement = array("!nid" => $node->nid);
      trigger_error(t("Image file missing for image node !nid", $translate_replacement), E_USER_ERROR);
    }
  }
}

function htmlvideo_content_sync_node_alter(&$node, $node_exists_locally) {
  if ($node->type == 'htmlvideo') {
    $types = htmlvideo_get_types();
    foreach ($types as $type => $data) {
      if ($node_exists_locally) {
        if (!empty($node->{$type})) {
          $current_file = db_result(db_query(
            "SELECT f.filepath
            FROM {htmlvideo} h
            INNER JOIN {files} f ON h.fid = f.fid
            WHERE h.nid = %d AND h.type = '%s'", $node->nid, $type));
          if ($current_file != $node->{$type}) {
            // A new image has been uploaded. Notify the image module
            error_log($type . " updated for node " . $node->nid);
            $node->{"new_$type"} = TRUE;
          }
        }
      }
      if ($data['required'] && (!file_exists($node->{$type})) && empty($node->{$type . '_external_url'})) {
        $translate_replacement = array("!nid" => $node->nid);
        trigger_error(t("File missing for htmlvideo node !nid", $translate_replacement), E_USER_ERROR);
      }
    }
  }
}

function flashvideo_content_sync_node_alter(&$node, $node_exists_locally) {
  if ($node->type == 'video') {
    // Keep it simple. Just delete the old file references before we
    // process new files
    db_query("DELETE FROM {flashvideo} WHERE nid=%d", $node->nid);
    db_query("DELETE FROM {upload} WHERE nid=%d", $node->nid);

    if ($node->files) {
      $new_files = array();
      foreach ($node->files as &$file) {
        // Skip all files not ending with flv or jpg, as these are not needed
        // on the presentation site.
        $arr = explode(".", $file['filename']);
        if (count($arr)) {
          $suffix = strtolower($arr[count($arr) - 1]);
        }
        if (!in_array($suffix, array('jpg', 'flv'))) {
          continue;
        }

        // Fix file name
        _ndla_content_sync_rewrite_filename($file);

        if (!file_exists($file['filepath'])) {
          $translate_replacement = array(
            '!filepath' => $file['filepath'],
            '!nid' => $node->nid,
          );
          trigger_error(t("File missing (!filepath) for flashvideo node !nid", $translate_replacement), E_USER_ERROR);
        }

        $file_state = _ndla_content_sync_write_record('files', $file, 'fid');

        $file['nid'] = $node->nid;
        $file['vid'] = $node->vid;

        $size = flashvideo_get_size($node);
        db_query("INSERT INTO {flashvideo} (fid, nid, oid, status, video_index, width, height, flags) VALUES (%d, %d, %d, %d, %d, %d, %d, %d)", 
                 $file['fid'], $file['nid'], $file['fid'], FLASHVIDEO_STATUS_CONVERTED, 0, $size['width'], $size['height'], 0);
        db_query("INSERT INTO {upload} (fid, nid, vid, description, list, weight) VALUES (%d, %d, %d, '%s', %d, %d)",
                 $file['fid'], $file['nid'], $file['vid'], $file['description'], $file['list'], $file['weight']);

        $new_files[$file['fid']] = $file;
      }

      $node->files = $new_files;
    }
  }
}

function ndla_apps_content_sync_node_alter(&$node, $node_exists_locally) {
  if ($node->type == 'ndla_app') {
    $files = array('large_icon', 'medium_icon', 'small_icon', 'extra_small_icon');
    foreach($files as $file) {
      _ndla_content_sync_rewrite_filename($node->$file);
      _ndla_content_sync_write_record('files', $node->$file, 'fid');
    }
  }
}

function flashnode_content_sync_node_alter(&$node, $node_exists_locally) {
  if ($node->type == 'flashnode') {
    // Fix file name
    _ndla_content_sync_rewrite_filename($node->flashnode);

    $node->flashnode['filemime'] = file_get_mimetype($node->flashnode['filepath']);
    $node->flashnode['filesize'] = filesize($node->flashnode['filepath']);

    if (!file_exists($node->flashnode['filepath'])) {
      $translate_replacement = array("!nid" => $node->nid);
      trigger_error(t("File missing for flashnode node !nid", $translate_replacement), E_USER_ERROR);
    }

    _ndla_content_sync_write_record('files', $node->flashnode, 'fid');
  }
}

function audio_content_sync_node_alter(&$node, $node_exists_locally) {
  if ($node->type == 'audio') {

    // XMLRPC has converted the file object to array, so we must convert it back
    $node->audio["file"] = (object) $node->audio["file"];

    // Fix file name
    _ndla_content_sync_rewrite_filename($node->audio["file"]);

    if (!file_exists($node->audio["file"]->filepath)) {
      $translate_replacement = array("!nid" => $node->nid);
      trigger_error(t("File missing for audio node !nid", $translate_replacement), E_USER_ERROR);
    }

    _ndla_content_sync_write_record('files', $node->audio["file"], 'fid');
  }
}


/**
 * Implements hook_content_sync_node_alter()
 *
 * NOTE! We retrieve the questions for each quiz manually. This means that
 * these are not contolled by the publishing system. This should be ok, since a
 * published quiz should never contain un-published questions.
 */
function quiz_content_sync_node_alter(&$node, $node_exists_locally) {
  require_once drupal_get_path('module', 'quiz') . '/quiz.module';

  if ($node->type == 'quiz') {
    // We have to ask the server about which questions belong to this quiz
    error_log("Fetching question list for quiz from server");
    $incoming_questions_raw = ndla_content_sync_xmlrpc('ndla.getQuizQuestions', $node->nid, $node->orig_vid);

    if (is_string($incoming_questions_raw)) {
      $incoming_questions = unserialize(gzuncompress(base64_decode($incoming_questions_raw)));
      // Retrieve the questions from the server
      error_log(sprintf("Fetching %d question nodes from server", count($incoming_questions)));
      $q_nodes = ndla_content_sync_fetch_nodes($incoming_questions, TRUE);
      if ($q_nodes === FALSE) {
        drupal_set_message(t('Fetching quiz questions from server failed'), 'error'); 
        return;       
      }
      $questions = array();
      foreach ($incoming_questions as $question) {
        // Create array containing objects of incoming questions that can be
        // processed by quiz_[set|update]_questions()
        $q = new stdClass();
        $q->nid = $question->nid;
        $q->vid = $q_nodes[$question->nid]->vid;
        $q->state = $question->question_status;
        $q->weight = $question->weight;
        $q->max_score = $question->max_score;
        $questions[] = $q;
      }
    }
    elseif (is_object($incoming_questions_raw) && $incoming_questions_raw->is_error) {
      $error = array(
        '!message' => $incoming_questions_raw->message,
        '!code' => $incoming_questions_raw->code,
      );
      drupal_set_message(t('Fetching quiz questions list failed: !message (Code: !code)', $error), 'error');
    }

    // Save questions in node object. This will be processed after the node has
    // been competely saved.
    $node->questions = $questions;
  }
  else if ($node->type == 'multichoice') {
    if (isset($node->alternatives) && is_array($node->alternatives)) {
      foreach ($node->alternatives as &$alternative) {
        unset($alternative['id']);
      }
    }
  }
}

function cookbook_content_sync_node_alter(&$node, $node_exists_locally) {
  if ($node->type == 'ingredient') {
    // Make sure cookbook units are written to the database to preserve the id
    if ($node->cookbook_ingredient_units) {
      foreach ($node->cookbook_ingredient_units as &$unit) {
        $unit = (object) $unit;
        $unit->ingredient_nid = $node->nid;
        $unit->ingredient_vid = $node->vid;

        _ndla_content_sync_write_record('cookbook_units', $unit, 'id');
      }
    }
  }
  else if ($node->type == 'recipe') {
    // Make sure cookbook ingredient images are written to the database to preserve the id
    if ($node->cookbook_recipe_images) {
      foreach ($node->cookbook_recipe_images as &$image) {
        $image = (object) $image;
        $image->recipe_nid = $node->nid;
        $image->recipe_vid = $node->vid;

        _ndla_content_sync_write_record('cookbook_recipe_images', $image, 'id');
      }
    }

    // Make sure cookbook ingredients are written to the database to preserve the id
    if ($node->cookbook_recipe_ingredients) {
      foreach ($node->cookbook_recipe_ingredients as &$ingredient) {
        $ingredient = (object) $ingredient;
        $ingredient->recipe_nid = $node->nid;
        $ingredient->recipe_vid = $node->vid;

        _ndla_content_sync_write_record('cookbook_recipe_ingredients', $ingredient, 'id');
      }
    }
  }
}

function content_content_sync_node_alter(&$node, $node_exists_locally) {
  if ($node->type == 'fil') {
    foreach ($node->field_vedlegg as &$file) {
      // Fix file name
      _ndla_content_sync_rewrite_filename($file);

      if (!file_exists($file['filepath'])) {
        $translate_replacement = array("!nid" => $node->nid);
        trigger_error(t("File missing for fil node !nid", $translate_replacement), E_USER_ERROR);
      }

      _ndla_content_sync_write_record('files', $file, 'fid');
    }
  }

  if($node->type == 'htmlvideo') {
    foreach ($node->field_userguide_thumbnail as &$file) {
      // Fix file name
      _ndla_content_sync_rewrite_filename($file);

      if (!file_exists($file['filepath'])) {
        $translate_replacement = array("!nid" => $node->nid);
        trigger_error(t("File missing for fil node !nid", $translate_replacement), E_USER_ERROR);
      }

      _ndla_content_sync_write_record('files', $file, 'fid');
    }
  }
}

/**
 * Implements hook_content_sync_node_alter()
 *
 * NOTE! We retrieve the frameworks manually. This means that these are not
 * contolled by the publishing system. This should be ok, since the frameworks
 * are merely containers for the eLectures.
 */
function amendor_electure_content_sync_node_alter(&$node, $node_exists_locally) {
  static $fetched_frameworks = array();

  if ($node->type == 'amendor_electure') {
    // Convert swf array to object.
    $node->swf = (object) $node->swf;

    // Fix file name
    _ndla_content_sync_rewrite_filename($node->swf);

    if (!file_exists($node->swf->filepath)) {
      $translate_replacement = array("!nid" => $node->nid);
      trigger_error(t("File missing for amendor_electure node !nid", $translate_replacement), E_USER_ERROR);
    }

    _ndla_content_sync_write_record('files', $node->swf, 'fid');
    $_SESSION['new_electure_swf'] = $node->swf;

    // Retreive eLecture framework

    // The amendor_eLecture module has been rewritten, so we need to add
    // the existing_framework here to make things save correctly
    $node->existing_framework = $node->framework;

    if (isset($node->existing_framework) && ($node->existing_framework > 0)) {
      // Only retreive once per sync session
      if (!isset($fetched_frameworks[$node->existing_framework])) {
        $arg = array(
          array(
            'nid' => $node->existing_framework,
          )
        );
        // NOTE! We retrieve the frameworks manually. This means that these are
        // not contolled by the publishing system, which should be ok.
        ndla_content_sync_fetch_nodes($arg);
        $fetched_frameworks[$node->existing_framework] = TRUE;
      }
    }
  }
  elseif ($node->type == 'amendor_electure_framework') {
    error_log("Fetching eLecture framework: {$node->nid}");
  }
}

function biblio_content_sync_node_alter(&$node, $node_exists_locally) {
  if ($node->type == 'biblio') {
    foreach ($node->biblio_contributors as &$contrib_list) {
      foreach ($contrib_list as &$contributor) {
        _ndla_content_sync_write_record('biblio_contributor_data', $contributor, 'cid');
      }
    }
  }
}

function ndla_utils_content_sync_node_alter(&$node, $node_exists_locally) {
  if ($node->type == 'fag') {
    
    if (isset($node->ndla_utils_ndla2010_theme_fag_header)) {
      // Fix file name
      _ndla_content_sync_rewrite_filename($node->ndla_utils_ndla2010_theme_fag_header);

      if (!file_exists($node->ndla_utils_ndla2010_theme_fag_header['filepath'])) {
        $translate_replacement = array("!nid" => $node->nid);
        trigger_error(t("Banner image file missing for fag node !nid", $translate_replacement), E_USER_ERROR);
      }

      $status = _ndla_content_sync_write_record('files', $node->ndla_utils_ndla2010_theme_fag_header, 'fid');
      if ($status == SAVED_UPDATED) {
        // The header file has not changed since last sync. Remove the header
        // from the node object to avoid deleting the header in ndla_utils
        unset($node->ndla_utils_ndla2010_theme_fag_header);
      }
    }
  }
}

function ndla_utils_disp_content_sync_node_alter(&$node, $node_exists_locally) {
  if($node->edit_date_created) {
    $nid = $node->nid;
    
    /* Remove old selections */
    $sql = "DELETE FROM {ndla_utils_edit_dates} WHERE nid = %d";
    db_query($sql, $nid);
    
    /* Add new selections */
    $sql = "INSERT INTO {ndla_utils_edit_dates} (nid, created, updated) VALUES (%d, %d, %d)";
    db_query($sql, $nid, $node->edit_date_created, $node->edit_date_updated);
    unset($node->edit_date_created);
    unset($node->edit_date_updated);
  }

  /**
   * Since og is disabled on cm the values will not be saved
   * in the og_ancestry table. But we are using that table to
   * see what fag a node is part of.
   * This code saves the values if the module is not enabled.
   * But if the module is uninstalled the table will be dropped
   * and this code will stop working.
   */
  if(!module_exists('og')) {
    db_query("DELETE FROM {og_ancestry} WHERE nid = %d", $node->nid);
    if(!empty($node->og_groups)) {
      foreach($node->og_groups as $gid) {
        db_query("INSERT INTO {og_ancestry} (nid, group_nid) VALUES (%d, %d)", $node->nid, $gid);
      }
    }
  }
}

function ndla_fag_taxonomy_content_sync_node_alter(&$node, $node_exists_locally) {
  if ($node->type == 'fag') {
    // On first insert, add mapping in ndla_fag_taxonomy_map to get correct
    // name on courses
    if (!$node_exists_locally) {
      // Get vocab ID for the course taxonomy
      $vocabuary = ndla_fag_taxonomy_get_default_vocabulary();
      $fag_vid = $vocabuary->vid;

      $fag_tid = 0;
      $largest = 0;
      foreach ($node->taxonomy as $tid) {
        // Loop over all the terms associated with the node.
        // Note: The whole term hierarchy is supplied for the course taxonomy so
        // we assume the one with the most ancestors is the leaf node
        $term_hierarchy = taxonomy_get_parents_all($tid);
        if ($term_hierarchy[0]->vid == $fag_vid) {
          if (count($term_hierarchy) > $largest) {
            $fag_tid = $tid;
            $largest = count($term_hierarchy);
          }
        }
      }

      if ($fag_tid !== 0) {
        $sql = "INSERT INTO {ndla_fag_taxonomy_map} VALUES(%d, %d)";
        db_query($sql, $node->nid, $fag_tid);
        $sql = "INSERT INTO {ndla_fag_taxonomy_term} VALUES(%d, %d)";
        db_query($sql, $fag_tid, 1);
      }
    }
  }
}


function ndla_menu_content_sync_node_alter(&$node, $node_exists_locally) {
  if ($node->type == 'nodemenu') {
    foreach($node->ndla_menu_files as &$file) {
      _ndla_content_sync_rewrite_filename($file);
      _ndla_content_sync_write_record('files', $file, 'fid');
    }
    $languages = ndla_menu_get_languages();
    foreach($node->ndla_menu as &$item) {
      foreach($languages as $lang => $name) {
        if(!empty($item->$lang->image_id)) {          
          $item->$lang->image_id = $node->ndla_menu_files[$item->$lang->image_id]->fid;
        }
      }
    }
  }
}


/**
 * Helper function that renames files in incoming content.
 *
 * When a file is uploaded to drupal, and there is a name collision, drupal
 * fixes this by appending numbers to the filename. In those cases the filename
 * specified in filepath differs from the one in filename. In these cases,
 * the file is renamed again when syncronized. By setting the filname attribute
 * equal to the filename in filepath, this is avoided.
 *
 * @param file
 *   A file object/array
 */
function _ndla_content_sync_rewrite_filename(&$file) {
  // Cast file to object if array
  $is_array = FALSE;
  if (is_array($file)) {
    $file = (object) $file;
    $is_array = TRUE;
  }

  if ($pos = strrpos($file->filepath, '/')) {
    $real_file = substr($file->filepath, ($pos + 1));
  }
  if (isset($real_file) && $real_file != $file->filename) {
    $file->filename = $real_file;
  }

  // Cast file back to array if it originally was an array
  if ($is_array) {
    $file = (array) $file;
  }
}
