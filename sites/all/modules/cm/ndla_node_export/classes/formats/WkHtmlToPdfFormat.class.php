<?php

class WkHtmlToPdfFormat {
  public $extension = 'pdf';
  protected $file_content;
  protected $cover_html;
  protected $html;
  protected $html_shards;

  public function setup($metadata = null, $cover_html = null) {
    $this->metadata = $metadata;
    if($cover_html) {
      $this->cover_html = $cover_html;
    }
  }
  public function input($html_shards) {
    $this->html_shards = $html_shards;
  }

  public function transform() {
    global $language;
    if(empty($this->html_shards)) {
      return FALSE;
    }
    require_once drupal_get_path('module', 'ndla_export') . '/libraries/wkhtmltopdf.php';

    $drupal_path = realpath('.');
    $wkhtmltopdf_binary = variable_get('ndla_export_wkhtmltopdf_bin', $drupal_path . '/sites/all/libraries/wkhtmltopdf/wkhtmltopdf-amd64');

    $this->pdf = new WkHtmlToPdf(array(
      'bin'           => $wkhtmltopdf_binary,
      'enable-external-links',
      'encoding' => 'UTF-8',
      'margin-top'    => 10,
      'margin-right'  => 10,
      'margin-bottom' => 10,
      'margin-left'   => 10,
      'image-quality' => 60,
      'image-dpi'     => 100,
      'outline', // Make Chrome not complain
      'footer-center' => '[page]/[topage]',
    ));

    $this->pdf->addToc(array(
     'xsl-style-sheet' => $drupal_path . '/' . drupal_get_path('module', 'ndla_export') . '/toc_stylesheet.html',
    ));
    

    // Add cover html if exists
    if(!empty($this->cover_html)) {
      $this->pdf->addCoverHTML($this->cover_html);
    }
    
    $max_open_files = 1; // 1024 - 100; // This is almost always the same on all Linux environments.
    $chunked_shards = array_chunk($this->html_shards, ceil(count($this->html_shards)/$max_open_files), TRUE);

    foreach($chunked_shards as $html_array) {
      $html = array();

      //Reset page option array
      $page_options = array();

      foreach($html_array as $html_data) {
         $html[] = $html_data['html'];
      }

      $html_pages = implode($html, '<div style="page-break-after: always;">&nbsp;</div>');

      if(strpos($html_pages, "Wirisformula") !== FALSE) {
        $page_options['javascript-delay'] = 120000; // MathJAX needs extra processing time
        $this->log('Proccessing MATH tag for node, will take 2 minutes per pdf');
      }

      $this->pdf->setPageOptions($page_options);
      $css = file_get_contents($drupal_path . '/' . drupal_get_path('module', 'ndla_export') . '/templates/page.css');
      $head = "<html>
                <head>";

      
      if(strpos($html_pages, "Wirisformula") !== FALSE) {
        $head .= "<script src='https://cdn.mathjax.org/mathjax/latest/MathJax.js'></script>
                    <script type='text/javascript'>
                      MathJax.Hub.Config({
                        config: ['MMLorHTML.js'],
                        jax: ['input/MathML','output/SVG'],
                        extensions: ['mml2jax.js', 'CHTML-preview.js']
                      });
                    </script>";
      }

      $head .= "<style>".$css."</style>
                </head>";
      $html = $head . $html_pages . '</body></html>';

      $this->pdf->addPage($html);
    }
    return TRUE;
  }

  public function output($filename) {
    $return = $this->pdf->saveAs($filename);
    return $return;
  }

  public function get_identifier_uri() {
    return 'http://ndla.no';
  }

  public function log($msg, $level = 'ok') {
    drush_log(__class__ . " :: " . $msg, $level);
  }
}
