
ndla_textselect_readspeaker_init = function($selector) {
  $selector.find('.ndla_textselect_readspeaker').click(ndla_textselect_readspeaker_event);
  $('.rspopup').hide();
}

// not used anymore.
ndla_textselect_readspeaker = function() {}

ndla_textselect_readspeaker_event = function(event){
  $('#textselect').hide();
  $('.rspopup').show();
  try{
    setTimeout(function() {
      $('.rsbtn_play').last().click();
    }, 500);    
  } 
  catch(e) {
    console.log("Fail when clicking RS-button: " + e);
  }
};