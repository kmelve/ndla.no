<?php
// $Id: readspeaker_hl.admin.inc,v 1.1 2010/03/24 09:43:11 rsp Exp $

/**
 * @file
 * Administrative page callbacks for the readspeaker module.
 *
 * @ingroup readspeaker
 */

/**
 * Implementation of hook_settings().
 * 
 * General settings form for ReadSpeaker
 * Layout settings form for ReadSpeaker
 * Node type selection form for ReadSpeaker
 */
function readspeaker_admin_settings() {

  $readspeaker_languages = readspeaker_language_list();
  $languages = language_list();
  $lang_list = array();
  foreach ($languages as $language) {
    if (array_key_exists($language->language, $readspeaker_languages)) {
      $lang_list[$language->language] = t($language->name);
    }
  }

  // Required settings for ReadSpeaker
  $form['general_settings'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#title' => t('General settings for ReadSpeaker'),
    '#description' => t('The ReadSpeaker module requires an own account at <a href="http://www.readspeaker.com" target="_blank">ReadSpeaker</a>.'),
  );
  $form['general_settings']['readspeaker_accountid'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter your ReadSpeaker ID'),
    '#default_value' => variable_get('readspeaker_accountid', ''),
    '#description' => t('Enter your ReadSpeaker ID from <a href="https:/app.readspeaker.com/portal" target="_blank">https:/app.readspeaker.com/portal</a>.'),
    '#required' => TRUE,
  );
  $form['general_settings']['readspeaker_langid'] = array(
    '#type' => 'select',
    '#title' => t('Language'),
    '#default_value' => variable_get('readspeaker_langid', ''),
    '#options' => $lang_list + array('all' => t('All languages')),
    '#description' => t('Select which language your ReadSpeaker account supports.'),
    '#required' => TRUE,
  );
  $form['general_settings']['readspeaker_prefer_uk'] = array(
    '#type' => 'checkbox',
    '#title' => t('Prefer british english'),
    '#default_value' => variable_get('readspeaker_prefer_uk', FALSE),
    '#description' => t('Check this option, if you prefer british english.'),
    '#required' => FALSE,
  );

  // Layout settings for input ReadSpeaker into node
  $form['layout_settings'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#title' => t('Layout settings'),
    '#description' => t('Options to control display position of readspeaker on your website.'),
  );
  $form['layout_settings']['readspeaker_buttonstyle'] = array(
    '#type' => 'textfield',
    '#title' => t('Style attribute for the ReadSpeaker Button.'),
    '#default_value' => variable_get('readspeaker_buttonstyle', FALSE),
    '#description' => t('Set a style attribute for the ReadSpeaker Button.'),
    '#required' => FALSE,
  );
  // What kinds of nodes do we want to read by ReadSpeaker
  $form['content_types'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#title' => t('Content types'),
  );
  $form['content_types']['readspeaker_nodes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content types'),
    '#default_value' => variable_get('readspeaker_nodes', array()),
    '#options' => array_map('check_plain', node_get_types('names')),
    '#description' => t('Select content types to read by ReadSpeaker.'),
  );

  return system_settings_form($form);
}
