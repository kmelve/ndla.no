<?php
// $Id: utdanning_rdf.admin.inc,v 1.5 2008/10/01 09:05:53 arto Exp $

/**
 * @file
 * @ingroup utdanning_rdf
 */


//////////////////////////////////////////////////////////////////////////////
// utdanning_rdf API settings form

function utdanning_rdf_admin_settings() {
	module_load_include('inc', 'utdanning_rdf', 'utdanning_rdf.pages'); 
  $form = array();
  $node_types =node_get_types();	
  $form['#tree'] = TRUE;
	$form['utdanning_rdf_view_name'] = array(
		'#title' => t('View name'),
		'#type' => 'textfield',
		'#default_value' => variable_get('utdanning_rdf_view_name', 'noderef'),
		'#description' => t('Enter the name of the view which will be used if ole_noderef is installed'),
	);
  	// variable: utdanning_rdf_<relates to this type>_<this type>
	if(!arg(4)){
		$form["utdanning_rdf_show_inverse"]=array(
      "#type" => "select",
      '#title' => t('Enable view of inverse'), 
      '#default_value' => variable_get('utdanning_rdf_show_inverse',0),
      '#options' => array(1 => t('Enable'), 0 => t('Disable')),
			'#description' => t('Enable output of the inverse of relations'),
		);
  }
	
	$types = node_get_types();
	$relations = utdanning_rdf_get_predicates();

	foreach($types as $type => $data) {
	  $default_vals = ndla_utils_var_get('utdanning_rdf_master_' . $type, array());
    $form['enabled_relations'][$type] = array(
      '#type' => 'fieldset',
      '#title' => $data->name,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    foreach($types as $to_type => $to_data) {
      $form['enabled_relations'][$type][$to_type] = array(
        '#type' => 'fieldset',
        '#title' => $to_data->name,
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );
      $form['enabled_relations'][$type][$to_type]['relations'] = array(
        '#type' => 'checkboxes',
        '#options' => $relations,
        '#default_value' => $default_vals[$to_type],
      );
    }

  }
	$form['submit']=array(
		'#type' => 'submit',
		'#value' => t('Save')
	);


	
	//$default_vals = ndla_utils_var_get('utdanning_rdf_master_relations', array());
	
  
  $form['submit']=array(
		'#type' => 'submit',
		'#value' => t('Save')
	);

	return $form;
}

/**
 * Implementation of hook_validate() for the form utdanning_rdf_admin_settings.
 */
function utdanning_rdf_admin_settings_validate($form, &$form_state) {
	if(empty($form_state['values']['utdanning_rdf_view_name'])) {
		form_set_error('utdanning_rdf_view_name', t('The name of the view cant be empty'));
	}
	
}

/**
 * Implementation of hook_submit() for the form utdanning_rdf_admin_settings.
 */
function utdanning_rdf_admin_settings_submit($form, &$form_state) {
	foreach($form_state['values']['enabled_relations'] as $type => $data) {
    $saved = array();
    foreach($data as $to_type => $to_data) {
      $saved[$to_type] = array_filter($to_data['relations']);
    }
    ndla_utils_var_set('utdanning_rdf_master_' . $type, $saved);
    
  }

  unset($form_state['values']['enabled_relations']);
  foreach($form_state['values'] as $key => $value) {
	  variable_set($key, $value);
	}
	drupal_set_message(t('Saved configuration'));
}

function utdanning_rdf_admin_settings_edit(){
	$form=array();
	$form["import"]=array(
		'#type' => 'textarea',
  		'#title' => t('Import'),
  		'#default_value' => '',
  		'#required' => TRUE,
		'#description' => t('Add new relations')
	);
	$form['submit']=array(
		'#type' => 'submit',
		'#value' => t('Start import')
	);
	return $form;
}

function utdanning_rdf_admin_settings_edit_validate($form, &$form_state){
	$data = rdf_unserialize($form_state['values']['import'], array('format' => 'turtle'));
	
	if(count($data)<1){		
		form_set_error("import", t("Error in code"));
	}

}
function utdanning_rdf_admin_settings_edit_submit($form, &$form_state) {
	//$data = rdf_unserialize($text, array('format' => 'turtle', 'uri' => $url));
	//$data = rdf_unserialize($form_state['values']['import'], array('format' => 'turtle'));
	
	$data=rdf_unserialize($form_state['values']['import'], array('format' => 'turtle'));
	$counter=0;	
	foreach ($data as $stmt) {
		if (call_user_func_array('rdf_insert', array_merge($stmt, array(array('graph' => $url, 'repository' => 'ndlaontologi'))))) {
	    	$counter++;
	    }
	}
	drupal_set_message(t('!count statements imported', array('!count' => $counter)));
				
	if (($errors = (count($data) - $counter))) {
	    drupal_set_message(t('!count statements not imported.', array('!count' => $errors)), 'error');
	}
}

function utdanning_rdf_admin_pred_edit(){
	$form=array();
	$query = "SELECT * FROM {rdf_resources} n WHERE locate('ndla',n.uri) > 0  ORDER BY n.uri";
	$noderesult = db_query($query);
	while($result=db_fetch_object($noderesult)){
		//print_r($result);
		$form[$result->rid]=array(
		  '#type' => 'fieldset',
		  '#title' => $result->uri,
		  '#weight' => 5,
		  '#collapsible' => TRUE,
		  '#collapsed' => TRUE,
		);
		$form[$result->rid][$result->rid]=array(
		  '#type' => 'checkbox',
		  '#title' => t('Delete'),
		  '#description' => t("WARNING. Use with care, all relations will be deleted. This action cannot be undone."),
		);
		$form[$result->rid]["rid:".$result->rid]=array(
		  '#type' => 'textfield',
		  '#title' => t("URI"),
		  '#default_value' => $result->uri,
		  '#size' => 60,
		  '#maxlength' => 128,
		  '#required' => TRUE,
		);
		
		$sub_query=db_query("SELECT * FROM {rdf_data_ndlaontologi} WHERE sid = %d AND pid =%d ", $result->rid, $result->rid);
		while($sub_results=db_fetch_object($sub_query)){
			$form[$result->rid]["did:".$sub_results->did]=array(
			  '#type' => 'textfield',
			  '#title' => $sub_results->lang,
			  '#default_value' => $sub_results->data,
			  '#size' => 60,
			  '#maxlength' => 128,
			  '#required' => TRUE,
			);
		}
	}
	$form["submit"]=array(
		"#type" => "submit",
		"#weight" => 999,
		"#value" => "Submit",
	);
	return $form;
	
}

function utdanning_rdf_admin_pred_edit_validate($form, &$form_state){
	//print "<pre>";	print_r(&$form_state); print "</pre>";
	foreach ($form_state['values'] as $label => $value){
		if(!isset($value)){
			form_set_error($label, t("Error in code"));
		}
	}
}
function utdanning_rdf_admin_pred_edit_submit($form, &$form_state) {
	foreach ($form_state['values'] as $label => $value){
		if(is_numeric($label) && $value==1){			
			db_query("DELETE FROM {rdf_resources} WHERE rid=%d", $label);
			db_query("DELETE FROM {rdf_data_ndlaontologi} WHERE pid=%d", $label);
			
		}
		if(substr($label,0, 4)=="did:"){
			//drupal_set_message($label." ".$value. " ". substr($label,4));
			db_query("UPDATE {rdf_data_ndlaontologi} SET data = '%s' WHERE did=%s LIMIT 1", $value, substr($label,4));	
			//UPDATE `relations`.`rdf_data_ndlaontologi` SET `data` = 'Fagstoffet' WHERE `rdf_data_ndlaontologi`.`did` =2 LIMIT 1 ;
		}
		if(substr($label,0, 4)=="rid:"){
			//db_query("UPDATE {rdf_data_ndlaontologi} SET data = '%s' WHERE did=%s LIMIT 1", $value, substr($label,4));
			db_query("UPDATE {rdf_resources} SET uri = '%s' WHERE rid=%s LIMIT 1", $value, substr($label,4));	
				//UPDATE `relations`.`rdf_resources` SET `uri` = 'http://www.ndla.no/ontologi#harRelatert2' WHERE `rdf_resources`.`rid` =1 LIMIT 1 ;
		}
		
	}
}

function utdanning_rdf_add_relation_form() {
  drupal_add_js(drupal_get_path('module', 'utdanning_rdf') . '/js/utdanning_rdf.admin.js');
  $types = node_get_types();
  $groups = array();
  foreach($types as $type => $data) {
    if(og_is_group_type($type)) {
      $groups[] = "'" . $type . "'";
    }
  }
  $group_options = array();
  
  $result = db_query("SELECT nid, title FROM {node} WHERE type IN (" . implode(", ", $groups) . ") ORDER BY title ASC");
  while($g = db_fetch_object($result)) {
    $group_options[$g->nid] = $g->title;
  }
  

  $form['relation_nb'] = array(
    '#title' => t('Name of relation in norwegian bokmål'),
    '#type' => 'textfield',
    '#required' => TRUE,
  );
  $form['relation_nn'] = array(
    '#title' => t('Name of relation in norwegian nynorsk'),
    '#type' => 'textfield',
    '#required' => TRUE,
  );
  $form['relation_en'] = array(
    '#title' => t('Name of relation in english'),
    '#type' => 'textfield',
    '#required' => TRUE,
  );
  $form['relation_machine'] = array(
    '#title' => t('Machine name of relation'),
    '#type' => 'textfield',
    '#required' => FALSE,
    '#description' => t('For example: hasInterestingData. If omitted the name will be based on the norwegian bokmål relation name.')
  );
  $form['inverse_relation_nb'] = array(
    '#title' => t('Name of inverse relation in norwegian bokmål'),
    '#type' => 'textfield',
    '#required' => TRUE,
  );
  $form['inverse_relation_nn'] = array(
    '#title' => t('Name of inverse relation in norwegian nynorsk'),
    '#type' => 'textfield',
    '#required' => TRUE,
  );
  $form['inverse_relation_en'] = array(
    '#title' => t('Name of inverse relation in english'),
    '#type' => 'textfield',
    '#required' => TRUE,
  );
  $form['inverse_relation_machine'] = array(
    '#title' => t('Machine name of inverse relation'),
    '#type' => 'textfield',
    '#required' => FALSE,
    '#description' => t('For example: isInterestingDataFor. If omitted the name will be based on the norwegian bokmål inverse relation name.')
  );
  $form['populate_subjects'] = array(
    '#prefix' => '<p style="display: block; font-weight: bold;">' . t('Enable this relation for') . '</p><input id="group_checker" type="checkbox" />&nbsp;' . t('All'),
    '#title' => "",
    '#type' => 'checkboxes',
    '#options' => $group_options,
    '#default_value' => array(),
    '#attributes' => array('class' => 'populate_subjects'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  
  return $form;
}

function utdanning_rdf_add_relation_form_validate($form, &$form_state) {
  $rel_name_nb = trim(check_plain($form_state['values']['relation_nb']));
  $rel_name_nn = trim(check_plain($form_state['values']['relation_nn']));
  $rel_name_en = trim(check_plain($form_state['values']['relation_en']));
  $rel_machine_name = $org_rel_machine_name = trim(check_plain($form_state['values']['relation_machine']));

  $inverse_rel_name_nb = trim(check_plain($form_state['values']['inverse_relation_nb']));
  $inverse_rel_name_nn = trim(check_plain($form_state['values']['inverse_relation_nn']));
  $inverse_rel_name_en = trim(check_plain($form_state['values']['inverse_relation_en']));
  $inverse_machine_name = $org_inverse_machine_name = trim(check_plain($form_state['values']['inverse_relation_machine']));
  
  if(empty($rel_machine_name)) {
    $rel_machine_name = $org_rel_machine_name = _utdanning_rdf_name_to_machine($rel_name_nb);
  }
  else {
    //Make sure the user follows the camelcase syntax
    $rel_machine_name = _utdanning_rdf_name_to_machine($rel_machine_name);
  }

  if(empty($inverse_machine_name)) {
    $inverse_machine_name = $org_inverse_machine_name = _utdanning_rdf_name_to_machine($inverse_rel_name_nb);
  }
  else {
    //Make sure the user follows the camelcase syntax
    $inverse_machine_name = _utdanning_rdf_name_to_machine($inverse_machine_name);
  }
  
  //Make sure the machine name isn't already taken
  if(utdanning_rdf_is_machine_name_taken($rel_machine_name)) {
    form_set_error('relation_machine', t('The machine name \'@relation\' is already taken.', array('@relation' => $org_rel_machine_name)));
  }
  //Same goes for the inverse relation machine name
  if(utdanning_rdf_is_machine_name_taken($inverse_machine_name)) {
    form_set_error('inverse_relation_machine', t('The inverse machine name \'@relation\' is already taken.', array('@relation' => $org_inverse_machine_name)));
  }
  
  //Machine names validated, lets validate the human readable names
  $fields = array(
    array('data' => $rel_name_nb, 'lang' => 'nb', 'field' => 'relation_nb'),
    array('data' => $rel_name_nn, 'lang' => 'nn', 'field' => 'relation_nn'),
    array('data' => $rel_name_en, 'lang' => 'en', 'field' => 'relation_en'),
    array('data' => $inverse_rel_name_nb, 'lang' => 'nb', 'field' => 'inverse_relation_nb'),
    array('data' => $inverse_rel_name_nn, 'lang' => 'nb', 'field' => 'inverse_relation_nn'),
    array('data' => $inverse_rel_name_en, 'lang' => 'en', 'field' => 'inverse_relation_en'),
    );

  foreach($fields as $field) {
    if(utdanning_rdf_is_name_taken($field['data'], $field['lang'])) {
      form_set_error($field['field'], t('The relation name \'@name\' already exists.', array('@name' => $field['data'])));
    }
  }
}

function utdanning_rdf_add_relation_form_submit($form, &$form_state) {
  global $language;
  $params = array();
  $params['rel_name_nb'] = trim(check_plain($form_state['values']['relation_nb']));
  $params['rel_name_nn'] = trim(check_plain($form_state['values']['relation_nn']));
  $params['rel_name_en'] = trim(check_plain($form_state['values']['relation_en']));
  $params['rel_machine_name'] = trim(check_plain($form_state['values']['relation_machine']));

  $params['inverse_rel_name_nb'] = trim(check_plain($form_state['values']['inverse_relation_nb']));
  $params['inverse_rel_name_nn'] = trim(check_plain($form_state['values']['inverse_relation_nn']));
  $params['inverse_rel_name_en'] = trim(check_plain($form_state['values']['inverse_relation_en']));
  $params['inverse_machine_name'] = trim(check_plain($form_state['values']['inverse_relation_machine']));
  
  $named_relation[$language->language]['relation'] = $params['rel_name_' .$language->language];
  $named_relation[$language->language]['inverse'] = $params['inverse_rel_name_' .$language->language];
  
  if(empty($params['rel_machine_name'])) {
    $params['rel_machine_name'] = _utdanning_rdf_name_to_machine($params['rel_name_nb']);
  }
  else {
    //Make sure the user follows the camelcase syntax
    $params['rel_machine_name'] = _utdanning_rdf_name_to_machine($params['rel_machine_name']);
  }

  if(empty($params['inverse_machine_name'])) {
    $params['inverse_machine_name'] = _utdanning_rdf_name_to_machine($params['inverse_rel_name_nb']);
  }
  else {
    //Make sure the user follows the camelcase syntax
    $params['inverse_machine_name'] = _utdanning_rdf_name_to_machine($params['inverse_machine_name']);
  }
  
  $params['uri'] = UTDANNING_RDF_BASE_URI;
  $params['uri_relation'] = UTDANNING_RDF_BASE_URI . '#' . $params['rel_machine_name'];
  $params['uri_inverse_relation'] = UTDANNING_RDF_BASE_URI . '#' . $params['inverse_machine_name'];
  
  $data = theme('utdanning_rdf_rdf_import', $params);
  $rdf_data = rdf_unserialize($data, array('format' => 'turtle'));
  
  foreach ($rdf_data as $stmt) {
		if (call_user_func_array('rdf_insert', array_merge($stmt, array(array('graph' => $url, 'repository' => 'ndlaontologi'))))) {
      $counter++;
    }
    else {
      drupal_set_message('Unable to insert statement: <pre>' . print_r($stmt, true) . '</pre>');
    }
	}
  rdf_insert(
      rdf_uri($params['uri_relation']),
      rdf_uri('http://www.w3.org/2002/07/owl#inverseOf'),
      rdf_uri($params['uri_inverse_relation']),
      array('repository' => 'ndlaontologi')
  );
  
  $enable_groups = array_filter($form_state['values']['populate_subjects']);
  if(count($enable_groups)) {
    utdanning_rdf_enable_relation_for_groups($params['uri_relation'], $enable_groups);
  }
  drupal_set_message(t('Relation @relation created and enabled on @num_groups subject(s)', array('@relation' => $named_relation[$language->language]['relation'], '@num_groups' => count($enable_groups))));
  $nids = array_filter($form_state['values']['populate_subjects']);
  $nodes = array();
  foreach($nids as $nid) {
    $node = ndla_utils_load_node($nid);
    $nodes[] = l($node->title, 'node/' . $nid);
  }
  $nodes = theme('item_list', $nodes);
  drupal_set_message(t('The nodes !nodes must be published if the new relationship shall start working.', array('!nodes' => $nodes)));
}

/**
 * Returns the machine readable name for a given string.
 *
 * @param $name
 *  Human readable string.
 * @return
 *  Machine readable string.
 */
function _utdanning_rdf_name_to_machine($name) {
  //Make everything lowercase.
  $name = mb_strtolower($name);
  
  //Transform scandinavian character into normal text.
  $name = str_replace(array('å','å','ö', 'æ' ,'ø'), array('a','a', 'o', 'a', 'o'), $name);
  
  //Remove all weird characters.
  $name = preg_replace("/[^a-z0-9 ]/", "", $name);
  
  //Turn the string into an array
  $name = explode(" ", $name);
  
  //Create the camel case syntax
  foreach($name as $index => $value) {
    if($index > 0) {
      $name[$index] = ucfirst($value);
    }
  }
  
  //Finally return a string.
  return implode("", $name);
}

/**
 * Checks if a given (machine) relation name already exists.
 *
 * @param $relation
 *  The relation to lookup
 * @return
 *  Boolean. TRUE if the name is taken, false otherwise.
 */
function utdanning_rdf_is_machine_name_taken($relation) {
  $rid = db_fetch_object(db_query("SELECT rid FROM {rdf_resources} WHERE uri = '%s'", UTDANNING_RDF_BASE_URI . "#" . check_plain($relation)))->rid;
  return !empty($rid);
}

/**
 * Checks if a given relation name already exists.
 *
 * @param $relation
 *  The relation to lookup
 * @return
 *  Boolean. TRUE if the name is taken, false otherwise.
 */
function utdanning_rdf_is_name_taken($name, $lang) {
  $did = db_fetch_object(db_query("SELECT did FROM {rdf_data_ndlaontologi} WHERE lang = '%s' AND data = '%s'", $lang, $name))->did;
  return !empty($did);
}

/**
 * Enables a relation for a given set of groups.
 *
 * @param $relation
 *  The relation to enable
 * @param $enable_groups
 *  Array with group id's (nids)  
 */
function utdanning_rdf_enable_relation_for_groups($relation, $enable_groups) {
  if(module_exists('ndla_group_access')) {
    foreach($enable_groups as $gid) {
      /*$node->nid = $gid;
      $node->vid = db_fetch_object(db_query("SELECT vid FROM {node} WHERE nid = %d", $gid))->vid; */
      $node = node_load($gid);
      $data = _ndla_group_access_load($node);
      foreach($data['ndla_group_access']['enabled_relations'] as $from_to => $to) {
        $to = $to['to'];
        foreach($to as $content_type => $relations) {
          $data['ndla_group_access']['enabled_relations'][$from_to]['to'][$content_type][$relation] = $relation;
        }
      }
    
      $node->ndla_group_access = $data['ndla_group_access'];
      $node->revision = TRUE;
      //_ndla_group_access_save($node);
      node_save($node);
      if(module_exists('ndla_publish_workflow')) {
        global $user;
        _ndla_publish_workflow_set_status($node->nid, $node->vid, PUBLISH_WORKFLOW_DRAFT, t('New relation added to subject. Node must be published to be affected'), $user->name); 
      }
    }
  }
  else {
    drupal_set_message(t('NDLA Group Access module is not enabled, unable to populate the group(s) with the new relation'));
  }
}

function utdanning_rdf_admin_move(){
  $predicates = utdanning_rdf_get_predicates();
  $form['warning'] = array(
    '#type' => 'item',
    '#value' => t('<b>WARNING</b>: Use with care, all relations vill be changed on all nodes. This action cannot be undone.'),
  );

  $form['from'] = array(
    '#prefix' => '<div id="from">',
    '#suffix' => '</div>',
    '#type' => 'select',
    '#title' => t('From'),
    '#options' => $predicates,
  );

  $form['to'] = array(
    '#prefix' => '<div id="to">',
    '#suffix' => '</div>',
    '#type' => 'select',
    '#title' => t('To'),
    '#options' => $predicates,
  );

  $form['submit']=array(
    '#prefix' => '<div id="submit">',
    '#suffix' => '</div>',
    '#type' => 'submit',
    '#value' => t('Move'),
  );
  
  $form['result']=array(
    '#prefix' => '<div id="result"><img align="top" src="/' . drupal_get_path('module', 'utdanning_rdf') . '/img/ajax-loader.gif"><span id="percent">0%</span><div id="list">',
    '#suffix' => '</div></div>',
    '#type' => 'item',
    '#value' => '',
  );
  
  drupal_add_js(drupal_get_path('module', 'utdanning_rdf') . '/js/utdanning_rdf.admin.move.js');
  drupal_add_css(drupal_get_path('module', 'utdanning_rdf') . '/css/utdanning_rdf.admin.move.css');

  return $form;
}

function utdanning_rdf_admin_move_count() {
  $from = check_plain($_REQUEST['from']);
  $results_from = rdf_query(NULL, $from, NULL, utdanning_rdf_rdf_options())->to_array();
  
  $to = utdanning_rdf_get_inverse($from);
  $results_to = rdf_query(NULL, $to, NULL, utdanning_rdf_rdf_options())->to_array();
  
  /**
   * Subtract 6 since 3 per relation is
   * relation information and not actual relations
   */
  drupal_json(array('count' => sizeof($results_from) + sizeof($results_to) - 6));
  exit();
}

function utdanning_rdf_admin_move_process() {
  $from = check_plain($_REQUEST['from']);
  $to = check_plain($_REQUEST['to']);
  
  $results = rdf_query(NULL, $from, NULL, utdanning_rdf_rdf_options())->to_array();

  $from_rid = db_fetch_object(db_query("SELECT rid FROM {rdf_resources} WHERE uri = '%s'", $from))->rid;
  $to_rid = db_fetch_object(db_query("SELECT rid FROM {rdf_resources} WHERE uri = '%s'", $to))->rid;
  
  $count = 0;
  $nodes = '';
  foreach($results as $result) {
    if(preg_match('/vid/', $result[0])) {
      $count++;
      
      $from_vid = str_replace('vid:', '', $result[0]);
      $to_nid = str_replace('nid:', '', $result[2]);
      
      $from_node = db_fetch_object(db_query("SELECT title, nid FROM {node_revisions} WHERE vid = '%d'", $from_vid));
      $from_nid = $from_node->nid;
      $from_title = $from_node->title;
      $to_title = db_fetch_object(db_query("SELECT title FROM {node} WHERE nid = '%d'", $to_nid))->title;
      
      $nodes .= l($from_title . " [vid:$from_vid]", "node/$from_nid") . ' ' . t('to') . ' ' . l($to_title, "node/$to_nid") . "<br>";
      
      $old_relation = md5("vid:{$from_vid}{$from}nid:{$to_nid}");
      $new_relation = md5("vid:{$from_vid}{$to}nid:{$to_nid}");
      
      /* Get rid/sid */
      $sid = db_fetch_object(db_query("SELECT rid FROM {rdf_resources} WHERE uri = '%s'", $result[0]))->rid;

      db_query("UPDATE {utdanning_rdf_data} SET relation = '%s' WHERE relation = '%s'", $new_relation, $old_relation);
      db_query("UPDATE {rdf_data_ndlaontologi} SET pid = '%d' WHERE pid = '%d' AND data = '%s' AND sid = '%s'", $to_rid, $from_rid, $result[2], $sid);
    }
    if($count == 10) {
      drupal_json(array('count' => $count, 'nodes' => $nodes, 'return' => 'x1'));
      exit();
    }
  }
  
  /* Repeat for inverse */
  $from = utdanning_rdf_get_inverse($from);
  $to = utdanning_rdf_get_inverse($to);
  
  $results = rdf_query(NULL, $from, NULL, utdanning_rdf_rdf_options())->to_array();
  
  $from_rid = db_fetch_object(db_query("SELECT rid FROM {rdf_resources} WHERE uri = '%s'", $from))->rid;
  $to_rid = db_fetch_object(db_query("SELECT rid FROM {rdf_resources} WHERE uri = '%s'", $to))->rid;
  
  foreach($results as $result) {
    if(preg_match('/vid/', $result[0])) {
      $count++;
      
      $from_vid = str_replace('vid:', '', $result[0]);
      $to_nid = str_replace('nid:', '', $result[2]);
      
      $from_node = db_fetch_object(db_query("SELECT title, nid FROM {node_revisions} WHERE vid = '%d'", $from_vid));
      $from_nid = $from_node->nid;
      $from_title = $from_node->title;
      $to_title = db_fetch_object(db_query("SELECT title FROM {node} WHERE nid = '%d'", $to_nid))->title;
      
      $nodes .= l($from_title . " [vid:$from_vid]", "node/$from_nid") . ' ' . t('to') . ' ' . l($to_title, "node/$to_nid") . "<br>";
      
      $old_relation = md5("vid:{$from_vid}{$from}nid:{$to_nid}");
      $new_relation = md5("vid:{$from_vid}{$to}nid:{$to_nid}");
      
      /* Get rid/sid */
      $sid = db_fetch_object(db_query("SELECT rid FROM {rdf_resources} WHERE uri = '%s'", $result[0]))->rid;

      db_query("UPDATE {utdanning_rdf_data} SET relation = '%s' WHERE relation = '%s'", $new_relation, $old_relation);
      db_query("UPDATE {rdf_data_ndlaontologi} SET pid = '%d' WHERE pid = '%d' AND data = '%s' AND sid = '%s'", $to_rid, $from_rid, $result[2], $sid);
    }
    if($count == 10) {
      drupal_json(array('count' => $count, 'nodes' => $nodes, 'return' => 'x2'));
      exit();
    }
  }
    
  drupal_json(array('count' => $count, 'nodes' => $nodes, 'return' => 'x3'));
  exit();  
}
