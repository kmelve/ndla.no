<?php

/* Implementation of hook_block() */
function ndla_dictionary_block($op = 'list', $delta = 0, $edit = array()) {
  global $language;
  switch ($op) {
    case 'list':
      $blocks[] = array(
        'info' => t('Dictionary'),
        'cache' => BLOCK_NO_CACHE,
      );
      return $blocks;
    case 'view':
      /* Check what fag we're in */
      $fag_node = ndla_utils_disp_get_course();
      $args = arg();
      if(empty($fag_node) && $args[0] == 'node' && is_numeric($args[1])) {
        $path = arg();
        $fag_node = db_fetch_object(db_query("SELECT group_nid as nid FROM {og_ancestry} WHERE nid = %d LIMIT 1", $args[1]));
      }
      $fag_id = $fag_node->nid;
      
      /* Is the fag using dictionary? */
      $types = array();
      $query = "SELECT type FROM {ndla_dictionary} GROUP BY type";
      $result = db_query($query);
      while ($item = db_fetch_object($result)) {
        $name = 'ndla_dictionary_' . $item->type . '_enabled';
        $fags = variable_get($name, array());
        if(!is_array($fags)) {
          $fags = array();
        }
        $fags = array_filter($fags);
        if(in_array($fag_id, $fags)) {
          $types[] = $item->type;
        }
      }
      
      if(empty($types))
        return;
    
      //TODO: Shall the panel be shown?
      $module_path = drupal_get_path('module', 'ndla_dictionary');
      drupal_add_css($module_path . '/ndla_dictionary.css');
      if (preg_match('/MSIE 6.0/', $_SERVER['HTTP_USER_AGENT'])) {
        drupal_add_css($module_path . '/ndla_dictionary-ie6.css');
      }
      drupal_add_js($module_path . '/ndla_dictionary.js');
      $opts = array('absolute' => FALSE,'ndla_utils_query_set' => TRUE);
      $json_url_list = url('ndla_dictionary.list', $opts);
      $json_url_details = url('ndla_dictionary.details', $opts);      
      drupal_add_js(
        array(
          'ndla_dictionary' => array(
            'json_list' => $json_url_list,
            'json_details' => $json_url_details,
            'list_length' => variable_get('ndla_dictionary_list_length', 15),
            'ajax_timeout' => variable_get('ndla_dictionary_ajax_timeout', 2000),
            'ajax_retries' => variable_get('ndla_dictionary_ajax_retries', 3),
          )
        ),
        'setting'
      );
      $blocks = array(
        'subject' => t('Dictionary'),
        'content' => theme('ndla_dictionary', $types),
      );
      return $blocks;
  }
}

/* Implementation of hook_theme() */
function ndla_dictionary_theme() {
  return array(
    'ndla_dictionary' => array(
      'template' => 'ndla_dictionary-block',
      'arguments' => array('types' => NULL),
    ),
  );
}

/* Implementation of hook_menu() */
function ndla_dictionary_menu() {
  $items['ndla_dictionary.list/%/%'] = array(
    'title' => 'NDLA Dictionary AJAX gateway - Dictionary',
    'page callback' => '_ndla_dictionary_fetch_entries',
    'access arguments' => array('access content'),
    'page arguments' => array(1,2),
    'type' => MENU_CALLBACK,
  );
  $items['ndla_dictionary.details/%'] = array(
    'title' => 'NDLA Dictionary AJAX gateway - Term detail',
    'page callback' => '_ndla_dictionary_fetch_entry',
    'access arguments' => array('access content'),
    'page arguments' => array(1),
    'type' => MENU_CALLBACK,
  );
  $items['admin/settings/ndla_dictionary'] = array(
    'title' => 'NDLA Dictionary',
    'description' => 'NDLA Dictionary settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ndla_dictionary_admin'),
    'access arguments' => array('administer ndla_dictionary'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}
 
/**
 * Fetches terms from the database and returns
 * them as a JSON list.
 *
 * @param string $search the search string
 * @param string $lang   the selected language
 * @return string        json
 */
function _ndla_dictionary_fetch_entries($search, $type) {
  $collate = '';
  if ($lang == 'no') {
    /* No norwegian callation exists, but danish is equal to norwegian. */
    $collate = ' COLLATE utf8_danish_ci';
  }
  $query = "SELECT
              id,
              term,
              additional_information
            FROM {ndla_dictionary}
            WHERE term LIKE '%s%' $collate
            AND type = '%s' ORDER BY term $collate";
            
  $result = db_query($query, $search, $type);
  drupal_set_header('Content-Type: text/plain; charset: utf-8');
  $data = array();
  while ($item = db_fetch_object($result)) {
    if(empty($item->additional_information))
      $item->additional_information = "";
    $data[] = array(
      'id' => $item->id,
      'term' => $item->term,
      'additional_information' => $item->additional_information,
    );
  }  
  echo json_encode(array('terms' => $data));
}

/*
 * Fetches a term from the database and returns
 * them as a JSON object.
 *
 * @param string $id the id of the term
 * @return string    json
 */
function _ndla_dictionary_fetch_entry($id) {
  $query = 'SELECT d.description, v.description variant FROM {ndla_dictionary} d
            LEFT JOIN {ndla_dictionary} v ON (v.id = d.variant_id) WHERE d.id = %d';
	$result = db_query($query, $id);
	$item = db_fetch_object($result);
	
	drupal_set_header('Content-Type: text/plain; charset: utf-8');
	
	if(empty($item->description)) {
	  $item->description = $item->variant;
	}
	
	echo json_encode(array('details' => array('data' => $item->description)));
}

function ndla_dictionary_admin() {
  $form = array(
    'ndla_dictionary_list_length' => array(
      '#type' => 'textfield',
      '#title' => t('List length'),
      '#description' => t('How many search results to display in the list'),
      '#size' => 3,
      '#maxlength' => 3,
      '#default_value' => variable_get('ndla_dictionary_list_length', 15),
    ),
    'ndla_dictionary_ajax_timeout' => array(
      '#type' => 'textfield',
      '#title' => t('Ajax timeout'),
      '#description' => t('How long time to wait for an Ajax call to return before retrying'),
      '#size' => 5,
      '#maxlength' => 5,
      '#field_suffix' => t('milliseconds'),
      '#default_value' => variable_get('ndla_dictionary_ajax_timeout', 2000),
    ),
    'ndla_dictionary_ajax_retries' => array(
      '#type' => 'textfield',
      '#title' => t('Ajax retries'),
      '#description' => t('Number of times an Ajax call should be called before bailing out'),
      '#size' => 3,
      '#maxlength' => 3,
      '#default_value' => variable_get('ndla_dictionary_ajax_retries', 3),
    ),
  );
  
  /* Get all subjects */
  $options = array();
  $subjects = ndla_utils_courses();
  foreach($subjects as $subject) {
    $options[$subject->nid] = $subject->name;
  }
  
  /* Get types */
  $query = "SELECT type FROM {ndla_dictionary} GROUP BY type";
  $result = db_query($query);
  while ($item = db_fetch_object($result)) {
    $name = 'ndla_dictionary_' . $item->type . '_enabled';
    $form[$name] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('Enable ' . $item->type . ' dictionary for subjects'),
      $name => array(
        '#type' => 'checkboxes',
        '#tree' => FALSE,
        '#default_value' => variable_get($name, array()),
        '#options' => $options,
      ),
    );
  }
  
  return system_settings_form($form);
}

/* Implementation of hook_perm(). */
function ndla_dictionary_perm() {
  return array(
    'administer ndla_dictionary',
  );
}