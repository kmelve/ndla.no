<?php
/** \addtogroup ndla_authors_migrate */

/**
 * @file
 * @ingroup ndla_authors_migrate
 */
 
/* Stub function
function ndla_authors_migrate_() {
  
}
*/

function ndla_authors_migrate_menu() {
  $items = array();
  
  $items['admin/settings/ndla_authors/migrate'] = array(
    'title' => t('NDLA Authors Migrate'),
		'page callback' => 'drupal_get_form',
		'page arguments' =>  array('ndla_authors_migrate_form'),
		'access arguments' => array('administer ndla_authors'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['admin/settings/ndla_authors/migrate/status'] = array(
    'page callback' => 'ndla_authors_migrate_status',
    'page arguments' => array(),
    'access arguments' => array('administer ndla_authors'),
    'type' => MENU_CALLBACK,
  );

  $items['admin/settings/ndla_authors/migrate/process'] = array(
    'page callback' => 'ndla_authors_migrate_process',
    'page arguments' => array(),
    'access arguments' => array('administer ndla_authors'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}


/**
 * Implementation of hook_form().
 */
function ndla_authors_migrate_form() {
  $form = array();
  
  drupal_add_js('misc/progress.js');
  
  $htm = '';

  $types = _ndla_authors_migrate_get_types();
  $diff = array_diff(array('Forfatter', 'Forlag', 'Opphavsmann', 'Redaksjonelt'), array_keys($types));
  $author_vocab = variable_get('ndla_authors_vocab', '');
  if ($author_vocab == '') {
    $htm .= t('A vocabulary must be assigned to the !link before migrating can start', array('!link' => l('NDLA Authors module', 'admin/settings/ndla_authors')));
  }
  else if (count($diff)) {
    $htm .= t('One or more terms (!missing) are missing from the !link, and must be added before migrating can start', array('!missing' => implode(', ', $diff), '!link' => l('NDLA Authors vocabulary', 'admin/content/taxonomy/' . $author_vocab)));
  }
  else {
    $htm .= '<script type="text/javascript">';
    $htm .= '
function start_process(mode) {
  $("#progress").empty();
  pb = new Drupal.progressBar(\'ndlaAuthorsProgressBar\');
  pb.setProgress("0", "processing...");
  $("#progress").append(pb.element);
  uri = \'' . base_path() . 'admin/settings/ndla_authors/migrate/\' + mode;
  $.get(uri , \'\', function(data) {
    pb.setProgress("100","");
    pb.stopMonitoring();
  });
  pb.startMonitoring(\'' . base_path() . 'admin/settings/ndla_authors/migrate/status\', 500);
  return false;
}
';

    $htm .= '</script>';
    $htm .= '<br/><br/><br/>';
    $htm .= '<input type="button" value="' . t('Migrate authors') . '" onclick="start_process(\'process\')" />';
    $htm .= '<div id="progress"></div>';
    $htm .= '<br/>';
  }

  $form['ndla_authors_synchronize'] = array('#value' => $htm );

  $form['#redirect'] = 'admin/settings/ndla_authors/migrate';
  return $form;
}


/**
 * Implementation of hook_form_submit().
 */
function ndla_authors_migrate_form_submit($form_id, $form) {
}


/**
 * Helper function for getting type mapping
 */
function _ndla_authors_migrate_get_types() {
  static $types = array();
  $terms = taxonomy_get_tree(variable_get('ndla_authors_vocab', ''));
  if (count($terms) && count($types == 0)) {
    foreach ($terms as $term) {
      $types[$term->name] = $term->tid;
    }
  }

  return $types;
}

/**
 * Callback function to show process status.
 */
function ndla_authors_migrate_status() {
  $cache = cache_get('ndla_authors_migrate_progress');
  $res = explode(";", $cache->data);
  print drupal_to_js(array('status' => TRUE, 'percentage' => number_format($res[0], 2), 'message' => $res[1]));
  exit();
}


/**
 * Callback function to start process.
 */
function ndla_authors_migrate_process() {
  set_time_limit(0);

  cache_set('ndla_authors_migrate_progress', '0.0;initializing...', 'cache', CACHE_TEMPORARY);
  
  $lim = "";
  //$lim = " WHERE nid<6824";
  $data = array();

  $result = db_query("SELECT * FROM {content_field_forfatter} " . $lim . " ORDER BY delta DESC");
  while ($author = db_fetch_object($result)) {
    if ($author->field_forfatter_nid) {
      $key = $author->nid . '_' . $author->vid;
      if (!isset($data[$key])) {
        $data[$key] = array('nid' => $author->nid, 'vid' => $author->vid);
      }
      if (!isset($data[$key]['forfatter'])) {
        $data[$key]['forfatter'] = array();
      }
      $data[$key]['forfatter'][] = $author->field_forfatter_nid;
    }
  }

  $result = db_query("SELECT * FROM {content_field_copyright} " . $lim . " ORDER BY delta DESC");
  while ($copyright = db_fetch_object($result)) {
    if ($copyright->field_copyright_nid) {
      $key = $copyright->nid . '_' . $copyright->vid;
      if (!isset($data[$key])) {
        $data[$key] = array('nid' => $copyright->nid, 'vid' => $copyright->vid);
      }
      if (!isset($data[$key]['copyright'])) {
        $data[$key]['copyright'] = array();
      }
      $data[$key]['copyright'][] = $copyright->field_copyright_nid;
    }
  }

  $result = db_query("SELECT * FROM {content_field_bearbeidetav} " . $lim . " ORDER BY delta DESC");
  while ($prepared = db_fetch_object($result)) {
    if ($prepared->field_bearbeidetav_nid) {
      $key = $prepared->nid . '_' . $prepared->vid;
      if (!isset($data[$key])) {
        $data[$key] = array('nid' => $prepared->nid, 'vid' => $prepared->vid);
      }
      if (!isset($data[$key]['bearbeidetav'])) {
        $data[$key]['bearbeidetav'] = array();
      }
      $data[$key]['bearbeidetav'][] = $prepared->field_bearbeidetav_nid;
    }
  }

  $num_nidvids = count($data);
  $progress = 0.0;
  $progress_step = 100.0 / $num_nidvids;
  cache_set('ndla_authors_migrate_progress', $progress . ';starting migration process...', 'cache', CACHE_TEMPORARY);
  $types = _ndla_authors_migrate_get_types();

  $count = 0;
  foreach ($data as $item) {
    db_query('DELETE FROM {ndla_authors} WHERE nid=%d AND vid=%d', $item['nid'], $item['vid']);
    cache_set('ndla_authors_migrate_progress', $progress . ';migrating...', 'cache', CACHE_TEMPORARY);
    $set_publisher = FALSE;
    if (isset($item['forfatter']) && isset($item['copyright'])) {
      $set_publisher = TRUE;
    }

    if (isset($item['forfatter'])) {
      foreach ($item['forfatter'] as $forfatter_nid) {
        db_query("INSERT INTO {ndla_authors} VALUES (%d, %d, %d, %d)", $item['nid'], $item['vid'], $types['Forfatter'], $forfatter_nid);
      }
    }

    if (isset($item['copyright'])) {
      foreach ($item['copyright'] as $copyright_nid) {
        if ($set_publisher) {
          $type = $types['Forlag'];
        }
        else {
          $type = $types['Opphavsmann'];
        }
        db_query("INSERT INTO {ndla_authors} VALUES (%d, %d, %d, %d)", $item['nid'], $item['vid'], $type, $copyright_nid);
      }
    }

    if (isset($item['bearbeidetav'])) {
      foreach ($item['bearbeidetav'] as $bearbeidetav_nid) {
        db_query("INSERT INTO {ndla_authors} VALUES (%d, %d, %d, %d)", $item['nid'], $item['vid'], $types['Redaksjonelt'], $bearbeidetav_nid);
      }
    }

    $progress += $progress_step;
  }
  cache_set('ndla_authors_migrate_progress', $progress . ';done migrating authors', 'cache', CACHE_TEMPORARY);

  exit();
}
