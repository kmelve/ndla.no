/**
 * @file
 * @ingroup ndla_apps
 */
$(document).ready(function(){
  $('#edit-start-button-text-wrapper, #edit-start-button-id-wrapper, #edit-start-url-wrapper, #edit-alias-start-url-wrapper').hide();
  if ($('#edit-start-type-0').is(':checked')) {
    $('#edit-start-url-wrapper').show();
    $('#edit-alias-start-url-wrapper').show();
  }
  else if ($('#edit-start-type-1').is(':checked')) {
    $('#edit-start-button-text-wrapper, #edit-start-button-id-wrapper').show();
  }
  $('#edit-start-type-0').click(function(){
    if ($(this).is(':checked')) {
      $('#edit-start-url-wrapper, #edit-alias-start-url-wrapper').show();
      $('#edit-start-button-text-wrapper, #edit-start-button-id-wrapper').hide();
    }
    else {
      $('#edit-start-url-wrapper, #edit-alias-start-url-wrapper').hide();
      $('#edit-start-button-text-wrapper, #edit-start-button-id-wrapper').show();
    }
  });
  $('#edit-start-type-1').click(function(){
    if ($(this).is(':checked')) {
      $('#edit-start-url-wrapper, #edit-alias-start-url-wrapper').hide();
      $('#edit-start-button-text-wrapper, #edit-start-button-id-wrapper').show();
    }
    else {
      $('#edit-start-url-wrapper, #edit-alias-start-url-wrapper').show();
      $('#edit-start-button-text-wrapper, #edit-start-button-id-wrapper').hide();
    }
  });
  $('#screenshot-browser').insertAfter('#edit-screenshot').css('margin-left', '5px');
  clickedid = 'edit-screenshot-wrapper';
  $('<input type=\"hidden\" id=\"edit-screenshot-autocomplete\" value=\"field_ingress_bilde\"/>').appendTo('body');
  runkode = function(text) {
    // Henter ut id fra [nid:1234]-blokka
    text = text.match(/\[nid:[\d]+\]$/)[0];
    text = text.substring(5, text.length - 1);
    $('#edit-screenshot').val(text);
    Lightbox.end('forceClose');
  };
  Lightbox.initList();
});
