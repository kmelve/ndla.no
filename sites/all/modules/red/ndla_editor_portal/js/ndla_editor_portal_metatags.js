Drupal.behaviors.ndla_editor_portal_metatags = function(context) {
  $('#edit-nodewords-metatags-description-value').after('<div id="metatag-counter"></div>');
  $('#edit-nodewords-metatags-description-value').keyup(ndla_editor_portal_metatags_count);
  ndla_editor_portal_metatags_count();
}

ndla_editor_portal_metatags_count = function() {
  if($('#edit-nodewords-metatags-description-value').length != 0) {
    var size = $('#edit-nodewords-metatags-description-value').val().length;
    if(size > 160) {
      $('#edit-nodewords-metatags-description-value').attr('style', 'color: red;');
    } else {
      $('#edit-nodewords-metatags-description-value').attr('style', 'color: black;');
    }
    $('#metatag-counter').html(size  + '/160');
  }
}