<?php

class JiraConnector {
  
  private $username;
  private $client;
  private $token;
  
  public function connect($username, $password) {
    $this->username = $username;
    try {
      $this->client = new SoapClient("http://bug.ndla.no/rpc/soap/jirasoapservice-v2?wsdl");
    } catch (Exception $error) {
      return $error->faultstring;
    }
    try {
      $this->token = $this->client->login($username, $password);
    } catch (Exception $error) {
      return $error->faultstring;
    }
    return TRUE;
  }
  
  public function getIssues($limit = 100) {
    try {
      $issues = $this->client->getIssuesFromJqlSearch($this->token, 'assignee = ' . $this->username . ' and status != Closed', $limit);
    } catch (Exception $error) {
      return false; 
    }
    return $issues;
  }
   
}