<?php global $user; ?>
<div class='ndla-editorial-blog-entry'>
  <h1><?php print $node->title; ?></h1>
  <?php 
    print check_markup(node_teaser($node->body, $node->format), $node->format, FALSE); 
  ?>
  <div class='readmore'><?php
    if(module_exists('comment')) {
      if($delete) {
        print l(t('Delete post'), 'node/' . $node->nid . '/delete', array('query' => array('destination' => 'user/' . $user->uid . '/ndla_editor_portal')));
      }
      print l(t('Read more and comment'), 'user/' . $user->uid . '/ndla_editor_portal/blog/' . $node->nid);
    }else{
      print l(t('Read more'), 'user/' . $user->uid . '/ndla_editor_portal/blog/' . $node->nid);
    }?>
  </div>
</div>