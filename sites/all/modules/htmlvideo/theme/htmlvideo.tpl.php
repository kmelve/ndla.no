<!-- Begin VideoJS -->
<div class="video-js-box">
  <!-- Using the Video for Everybody Embed Code http://camendesign.com/code/video_for_everybody -->
  <video id="htmlvideo_<?php print $light_node->nid?>" class="video-js" width="<?php print $attributes['width']?>" height="<?php print $attributes['height']?>"<?php print $attributes['extra_video_attributes']?>>
    <source src="<?php print $light_node->htmlvideo_mp4_url?>" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
    <?php if (!empty($light_node->htmlvideo_webm_url)): ?>
      <source src="<?php print $light_node->htmlvideo_webm_url?>" type='video/webm; codecs="vp8, vorbis"' />
    <?php endif ?>
    <?php if (!empty($light_node->htmlvideo_ogv_url)): ?>
      <source src="<?php print $light_node->htmlvideo_ogv_url?>" type='video/ogg; codecs="theora, vorbis"' />
    <?php endif; ?>
    <!-- Flash Fallback. Use any flash video player here. Make sure to keep the vjs-flash-fallback class. -->
    <object id="flash_fallback_1" class="vjs-flash-fallback" width="<?php print $attributes['width']?>" height="<?php print $attributes['height']?>" type="application/x-shockwave-flash"
      data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf">
      <param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
      <param name="allowfullscreen" value="true" />
      <param name="flashvars" value='config={"playlist":[{"url": "<?php print $light_node->htmlvideo_mp4_url?>","autoPlay":false,"autoBuffering":true}]}' />
      <?php if (!empty($light_node->htmlvideo_poster_url)): ?>
        <!-- Image Fallback. -->
        <img src="<?php print $light_node->htmlvideo_poster_url?>" width="<?php print $light_node->width?>" height="<?php print $light_node->height?>" alt="<?php print t('Poster Image') ?>"
          title="<?php print t('No video playback capabilities.')?>" />
      <?php else: ?>
        <?php print t('No video playback capabilities.')?>
      <?php endif ?>
    </object>
  </video>
  <!-- Download links provided for devices that can't play video in the browser. -->
  <p class="vjs-no-video"><strong><?php print t('Download Video')?>:</strong>
    <a href="<?php print $light_node->htmlvideo_mp4_url?>">mp4</a>,
    <?php if (!empty($light_node->htmlvideo_ogv_url)): ?>
      <a href="<?php print $light_node->htmlvideo_ogv_url?>">Ogg</a>
    <?php endif ?>
    <?php if (!empty($light_node->htmlvideo_webm_url)): ?>
      <a href="<?php print $light_node->htmlvideo_webm_url?>">WebM</a>,
    <?php endif ?>
  </p>
</div>
<!-- End VideoJS -->