CKEDITOR.plugins.add( 'ndla_wiris2', {
  icons: 'wirisbutton',
  init: function( editor ) {
    //Plugin command.
    editor.addCommand('ndla_wiris', new CKEDITOR.dialogCommand('ndlaWiris', {
      refresh: function (editor, path) {
        var found = false,
          selection = editor.getSelection(),
          element = selection.getStartElement();
        if ( element ) {
          element = element.getAscendant( 'span', true );
        }
        if (element && element.hasClass('Wirisformula')) {
          // selection.selectElement(element);
          found = true;
        }
        // Highlight button if within a formula.
        this.setState(found ? CKEDITOR.TRISTATE_ON : CKEDITOR.TRISTATE_OFF);
        editor.inWirisFormula = found;
      },
      contextSensitive: true
    }));

    // Add the custom toolbar buttons, which fires the above command..
    editor.ui.addButton( 'WirisButton', {
      label: 'Math',
      command: 'ndla_wiris'
    });

    editor.on( 'contentDom', function (ev) {
      editor.document.on('keydown', function (ev) {
        if (editor.inWirisFormula) {
          // TODO: If Enter is pressed, open dialog.
          // Don't block arrow keys, pg up/down, and F1-F12
          var k = ev.data.getKey();
          console.log(k);
          if ((k > 32 && k < 41) || (k > 111 && k < 124))
            return true;

          ev.data.preventDefault(true);
          return false;

        }
      });
    });

    // Finally, add the dialog.
    CKEDITOR.dialog.add('ndlaWiris', function(editor) {
      var applet = undefined;
      var mathml = "";
      var setAppletMathml = function () {
        // It takes a little while until the applet is properly loaded.  Hence
        // the retries.
        if (!applet) {
          applet = ns.$('#wiris-wrapper').contents().find('#applet')[0];
        }

        try {
          if (applet && applet.isActive && applet.isActive()) {
            applet.setContent(mathml);
          }
          else {
            setTimeout(setAppletMathml, 50);
          }
        }
        catch (e) {
          if (applet.isActive()) {
            applet.setContent(mathml);
          }
          else {
            setTimeout(setAppletMathml, 50);
          }
        }
      };

      return {
        title: 'Math editor',
        minWidth: 400,
        minHeight: 360,
        resizable: CKEDITOR.DIALOG_RESIZE_WIDTH,
        contents: [
          {
            id: 'tab1',
            label: 'First Tab',
            title: 'First Tab',
            elements: [
              {
                id: 'mathinput',
                type: 'html',
                // Height and overflow hides internal buttons from the
                // wiris_wysiwyg module.
                html: '<iframe id="wiris-wrapper" style="height: 370px; width: 500px; overflow: hidden;" src="' + Drupal.settings.basePath + 'index.php?q=wiris/editor/edit-paragraphs-0-left-field"></iframe>',
                setup: function (element) {
                  mathml = element.getHtml();
                  setAppletMathml();
                },
                commit: function (element) {
                  var content = applet.getContent();
                  element.setHtml(content);
                }
              }
            ]
          }
        ],
        onLoad: function() {
          // Keep this empty onLoad. Without it, saving seems to be bugged a
          // bit.
        },
        onShow: function () {
          var selection = editor.getSelection(),
            element = selection.getStartElement();

          if (element) {
            element = element.getAscendant('span', true);
          }
          if (element && element.hasClass('Wirisformula')) {
            this.insertMode = false;
          } else {
            element = editor.document.createElement('span');
            element.addClass('Wirisformula');
            this.insertMode = true;
          }
          this.element = element;
          this.setupContent(this.element);
        },
        onOk: function () {
          this.commitContent(this.element);
          if (this.insertMode) {
            editor.insertElement(this.element);
          }
        }
      };
    });
  }
});
