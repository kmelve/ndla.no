<?php

/**
 * @file
 *  ndla_h5p.module php file
 *  Drupal module ndla_h5p. Modifies the h5p module to work with the NDLA site.
 */

/**
 * Implementation of hook_menu().
 */
function ndla_h5p_menu() {
  $items = array();

//  $items['ndla-h5p/istribute-url'] = array(
//    'title' => 'NDLA H5P Istribute URL',
//    'page callback' => 'ndla_h5p_istribute_key_callback',
//    'page arguments' => array(2),
//    'access arguments' => array('create h5p'),
//    'type' => MENU_CALLBACK,
//  );

  $items['ndla-h5p/node-data/%'] = array(
    'title' => 'NDLA H5P Node Data',
    'page callback' => 'ndla_h5p_node_data_callback',
    'page arguments' => array(2),
    'access arguments' => array('create h5p'),
    'type' => MENU_CALLBACK,
  );

  $items['ndla-h5p/node-params/%'] = array(
    'title' => 'NDLA H5P Node Params',
    'page callback' => 'ndla_h5p_get_node_params_callback',
    'page arguments' => array(2),
    'access arguments' => array('create h5p'),
    'type' => MENU_CALLBACK,
  );

  $items['ndla-h5p/fetch-node/%'] = array(
    'title' => 'Fetch node (made for the arsHjul H5P)',
    'page callback' => 'ndla_h5p_fetch_node',
    'page arguments' => array(2),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Callback that fetches some data for a node (title, viewUrl and imageUrl)
 * @param  [string] $nid_list Commaseparated list of nids
 */
function ndla_h5p_fetch_node($nid_list) {
  $nids = explode(',', $nid_list);
  $result = array();

  foreach($nids as $nid) {
    $node = node_load($nid);
    
    if ($node === FALSE) {
      continue;
    }

    $result[$nid] = array(
      'title' => $node->title,
      'viewURL' => url('node/' . $node->nid)
    );
    // If field_ingress_bilde is present, add the imageURLs:
    if(isset($node->field_ingress_bilde) && isset($node->field_ingress_bilde[0])) {
      $image = node_load($node->field_ingress_bilde[0]['nid']);
      $result[$nid]['imageURLs'] = $image->images;
    }
  }

  drupal_set_header('Content-type: application/json');
  print json_encode($result);
}

/*
 * Implements hook_alter_h5p_library_list
 * @method ndla_h5p_alter_h5p_library_list
 * @param  [array]                          $libraries Array of library objects
 * @return [array]
 */
function ndla_h5p_alter_h5p_library_list($libraries) {
  $modified_libraries = array();
  $lookup = array(
    'H5P.CoursePresentation' => 'Fagpresentasjon',
    'H5P.InteractiveVideo' => 'Interaktiv video',
    'H5P.Blanks' => 'Fyll ut',
    'H5P.MultiChoice' => 'Flervalg',
    'H5P.SingleChoiceSet' => 'Miniquiz',
    'H5P.QuestionSet' => 'Spørsmålssett',
    'H5P.DragQuestion' => 'Dra og slipp objekt',
    'H5P.Summary' => 'Oppsummering',
    'H5P.DragText' => 'Dra og slipp tekst',
    'H5P.MarkTheWords' => 'Klikk på ord',
    'H5P.Dialogcards' => 'Dialogkort',
    'H5P.Flashcards' => 'Vendekort',
    'H5P.Boardgame' => 'Brettspill',
    'H5P.Timeline' => 'Tidslinje',
    'H5P.ImageHotspots' => 'Bilde med tekstpunkt',
    'H5P.ImageHotspotQuestion' => 'Bildepunkt-spørsmål',
    'H5P.IFrameEmbed' => 'Iframe',
    'H5P.DocumentationTool' => 'Dokumentasjonsverktøy',
    'H5P.arsHjul' => 'Ressurshjul',
    'H5P.Image' => 'Bilde',
    'H5P.StandardPage' => 'Fleksibel side',
    'H5P.GoalsPage' => 'Målside',
    'H5P.GoalsAssessmentPage' => 'Måloppnåelse-side',
    'H5P.Text' => 'Tekst',
    'H5P.AdvancedText' => 'Tekst',
    'H5P.DocumentExportPage' => 'Eksportside',
    'H5P.TextInputField' => 'Tekst og innskrivingsfelt'
  );

  // Reorder / set new title:
  foreach ($lookup as $machine_name => $title) {
    foreach ($libraries as $key => $library) {
      if ($machine_name === $library->name) {
        // Set new title:
        $library->title = $title;
        // Add to modified list:
        $modified_libraries[] = $library;
        // Remove from original list:
        unset($libraries[$key]);
      }
    }
  }

  // Libraries not in lookup table, is added to the end of modified list:
  return array_merge($modified_libraries, $libraries);
}


/**
 * Callback that generates a signed istribute key.
 *
 * @param type $key
 */
//function ndla_h5p_istribute_key_callback($key = NULL) {
//  if ($key === NULL) {
//    $key = uniqid('H5P-');
//  }
//
//  $appId = variable_get('istribute_appid', '');
//  $appCode = variable_get('istribute_appkey', '');
//
//  $path = '/v1/video/uploader/iframe/?appId=' . $appId . '&videoId=' . $key . '&unique=' . mt_rand(0,999999) . '.' . microtime(TRUE);
//  $path .= '&signature=' . hash_hmac('md5', $path, $appCode);
//
//  $data = array(
//    'appId' => $appId,
//    'url' => 'http://api.istribute.com' . $path,
//  );
//  drupal_set_header('Content-Type: application/json; charset=utf-8');
//  echo str_replace(array('<', '>', '&'), array('\\u003c', '\\u003e', '\\u0026'), json_encode($data));
//}

/**
 * Implementation of hook_form_alter()
 */
function ndla_h5p_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'h5p_content_node_form') {
    $form['#after_build'][] = 'ndla_h5p_node_form_process';
  }
  elseif ($form_id === 'contentbrowser_search_form') {
    $form['h5p_type'] = array(
      '#type' => 'hidden',
      '#default_value' => !empty($_REQUEST['h5p_type']) ? $_REQUEST['h5p_type'] : ''
    );
    if (!empty($_REQUEST['h5p_type']) && isset($_REQUEST['node_type'])) {
      $form['node_type'] = array(
        '#type' => 'hidden',
        '#default_value' => $_REQUEST['node_type']
      );
    }
  }
  else if($form_id == 'contentbrowser_get_details_form' && $form['#node']->type == 'h5p_content') {
    $ops = array(0 => t('No'), 1 => t('Yes'));
    $form['fullscreen'] = array(
      '#title' => t('Open in fullscreen'),
      '#type' => 'select',
      '#options' => $ops,
      '#attributes' => array('class' => 'postable'),
      '#default_value' => !empty($form['#tag']['fullscreen']) ? $form['#tag']['fullscreen'] : 0,
      '#weight' => 65,
    );
  }
}

function ndla_h5p_contentbrowser_available_attributes_alter(&$attributes) {
  $attributes[] = 'fullscreen';
}

/**
 * Implementation of hook_contentbrowser_alter_query().
 */
function ndla_h5p_contentbrowser_alter_query(&$query, &$joins, &$where) {
  if (isset($_POST['h5p_type']) && $_POST['h5p_type'] !== '') {
    $lib_parts = explode(' ', $_POST['h5p_type']);
    $lib_name = $lib_parts[0];
    $lib_version = explode('.', $lib_parts[1]);
    $joins[] = 'JOIN {h5p_nodes} hn ON n.nid = hn.nid';
    $joins[] = 'JOIN {h5p_libraries} hl ON hn.main_library_id = hl.library_id';
    $where[] = "hl.machine_name = '" . db_escape_string($lib_name) . "'";
    $where[] = "hl.major_version = " . db_escape_string($lib_version[0]);
    $where[] = "hl.minor_version = " . db_escape_string($lib_version[1]);
    $where[] = "hl.runnable = 1";
  }
}

/**
 * Form theme function
 */
function ndla_h5p_node_form_process($form, &$form_state) {

//  drupal_add_js($module_path . '/js/h5peditor-contentbrowser.js');
//  drupal_add_js($module_path . '/js/h5peditor-includination.js');
//  drupal_add_js($module_path . '/js/h5peditor-html-addon.js');

//  drupal_add_js($module_path . '/js/ndla_h5p.js');
//  drupal_add_css($module_path . '/css/ndla_h5p_edit.css');
//  drupal_add_css($module_path . '/css/ndla-h5p-content.css');

  $module_path = drupal_get_path('module', 'ndla_h5p');
  $cache_buster = '?' . variable_get('css_js_query_string', '');
  drupal_add_js(array(
    'h5peditor' => array(
      'wirisPath' => base_path() . $module_path . '/ckeditor_plugins/ndla_wiris2/',
      'assets' => array(
        'js' => array(
          '/' . $module_path . '/js/h5peditor-contentbrowser.js' . $cache_buster,
          '/' . $module_path . '/js/h5peditor-includination.js' . $cache_buster,
          '/' . $module_path . '/js/h5peditor-html-addon.js' . $cache_buster,
          '/' . $module_path . '/js/ndla_h5p.js' . $cache_buster,
        ),
        'css' => array(
          '/' . $module_path . '/css/ndla-h5p-edit.css' . $cache_buster,
          '/' . $module_path . '/css/ndla-h5p.css' . $cache_buster,
        )
      )
    )
  ), 'setting');

  return $form;
}

/**
 * Find all the nodes we have used and "tell" contentbrowser about it.
 *
 * @param object $node
 */
function ndla_h5p_insert_relations($node) {
  $counter = 0;
  $matches = array();
  preg_match_all('/"nodeId":"(\d+)"/', $node->json_content, $matches);

  if (count($matches[1]) !== 0) {
    // Find already existing relations to prevent duplicates.
    $result = db_query("SELECT nid FROM {contentbrowser_relations} WHERE used_by_vid = %d", $node->vid);
    $inserted = array();
    while ($node_id = db_result($result)) {
      $inserted[$node_id] = $node_id;
    }

    foreach ($matches[1] as $match) {
      if (isset($inserted[$match]) === FALSE) {
        db_query("INSERT INTO {contentbrowser_relations}
                  (nid, used_by_nid, used_by_vid)
                  VALUES(%d, %d, %d)",
                 $match, $node->nid, $node->vid);

        $inserted[$match] = $match;
        $counter++;
      }
    }
  }

  return $counter;
}

/**
 * Implementation of hook_nodeapi()
 */
function ndla_h5p_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  if ($op === 'update' || $op === 'insert' || $op === 'delete') {
    // TODO: Look for nid in json_content and set filtered = '' ?
    // This node could be used in a h5p, which means we must clear its cache when updating this node.
  }

  switch ($node->type) {
    case 'h5p_content':
      switch ($op) {
        case 'insert':
        case 'update':
          if (module_exists('contentbrowser')) {
            ndla_h5p_insert_relations($node);
          }
          break;

        case 'load':
          $semantics = ndla_h5p_get_semantics($node->main_library);
          $params = json_decode($node->json_content);
          ndla_h5p_process_params($semantics, $params);
          $node->json_content = json_encode($params);
          break;

        case 'view':
          // If not teaser and has library.
          if (!$a3 && isset($node->main_library)) {
            $node->content['h5p']['#value'] = '<div class="ndla-h5p-container">' . $node->content['h5p']['#value'] . "</div>";
            $node->content['h5p']['#weight'] = 40;
          }
          break;
      }
      break;
  }
}

function ndla_h5p_h5p_scripts_alter(&$scripts, $libraries, $embed_type) {
  $module_path = drupal_get_path('module', 'ndla_h5p');
  $version = '?' . variable_get('css_js_query_string', '0');

  /* Add MathJax so that math ml is rendered correctly. */
  if ($embed_type == 'iframe') {
    $scripts[] = (object) array(
      'path' => '//cdn.mathjax.org/mathjax/latest/MathJax.js',
      'version' => '?config=TeX-AMS-MML_HTMLorMML'
    );
  }
  else {
    drupal_set_html_head('<script type="text/javascript" src="//cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>');
  }

  /* Initialization of MathJax */
  $scripts[] = (object) array(
    'path' => $module_path . '/js/ndla-h5p-view.js',
    'version' => $version
  );
}

function ndla_h5p_h5p_styles_alter(&$styles, $libraries, $embed_type) {
  $module_path = drupal_get_path('module', 'ndla_h5p');
  $version = '?' . variable_get('css_js_query_string', '0');
  $styles[] = (object) array(
    'path' => $module_path . '/css/ndla-h5p.css',
    'version' => $version
  );
  $styles[] = (object) array(
    'path' => $module_path . '/css/ndla-h5p-iframe.css',
    'version' => $version
  );
}

/**
 * Get semantics from the special combination of library name and version.
 *
 * @param array $library_uberName
 * @return array
 */
function ndla_h5p_get_semantics($library) {
  // Why isn't library an object?
  $h5p = _h5p_get_instance('core');
  return $h5p->loadLibrarySemantics(isset($library['name']) ? $library['name'] : $library['machineName'], $library['majorVersion'], $library['minorVersion']);
}

/**
 * Implementation of hook_alter_h5p_params()
 */
function ndla_h5p_process_params($semantics, &$params) {
  for ($i = 0, $s = count($semantics); $i < $s; $i++) {
    $field = $semantics[$i];

    if (!isset($params->{$field->name})) {
      continue;
    }

    $result = ndla_h5p_process_params_field($field, $params->{$field->name});
    if ($result !== FALSE) {
      if (isset($params->copyright) && gettype($params->copyright) === gettype($result)) {
        $params->copyright = $result;
      }
      if (isset($params->alt) && gettype($params->alt) === gettype($result->title)) {
        $params->alt = $result->title; // For images
      }
    }
  }
}

/**
 * Process params for a field
 *
 * @param object $field
 * @param mixed $params
 */
function ndla_h5p_process_params_field($field, &$params) {
  switch ($field->type) {
    case 'image':
    case 'video':
    case 'audio':
      if (isset($params->nodeId)) {
        $result = ndla_h5p_get_node_data($params->nodeId);
      }
      elseif (is_array($params) && isset($params[0]->nodeId)) {
        $result = ndla_h5p_get_node_data($params[0]->nodeId);
      }

      if (isset($result)) {
        if (isset($result->error)) {
          $params = $result->error;
        }
        else {
          $params = $result->data;

          unset($result->copyright->year);
          if ($result->copyright->license === '') {
            unset($result->copyright->license);
          }
          if ($result->copyright->author === '') {
            unset($result->copyright->author);
          }
          if ($result->copyright->source === '') {
            unset($result->copyright->source);
          }

          if (is_array($params)) {
            $params[0]->copyright = $result->copyright;
          }
          else {
            $params->copyright = $result->copyright;
          }

          return $result->copyright;
        }
      }
      break;

    case 'library':
     if (isset($params->library) && isset($params->params)) {
        if (isset($params->nodeId)) {
          $node = node_load($params->nodeId);
          if ($params->library === 'H5P.Link 1.0') {
            if ($node->type === 'lenke' && isset($node->field_url) && isset($node->field_url[0]) && isset($node->field_url[0]['url']) && trim($node->field_url[0]['url']) !== '') {
              $url = $node->field_url[0]['url'];
              if (isset($node->field_url[0]['title']) && $node->field_url[0]['title'] !== '') {
                $title = $node->field_url[0]['title'];
              }
            }
            else {
              $url = url('node/' . $node->nid);
            }
            if (!isset($title)) {
              $title = $node->title;
            }

            $params->params = array(
              'title' => $title,
              'url' => $url,
            );
          }
          else {
            $params->params = json_decode($node->json_content);
          }
        }
        else {
          ndla_h5p_process_params(ndla_h5p_get_semantics(h5peditor_get_library_property($params->library)), $params->params);
        }
      }
      break;

    case 'group':
      if (isset($params)) {
        if (count($field->fields) === 1) {
          ndla_h5p_process_params_field($field->fields[0], $params);
        }
        else {
          ndla_h5p_process_params($field->fields, $params);
        }
      }
      break;

    case 'list':
      if (is_array($params)) {
        for ($i = 0, $s = count($params); $i < $s; $i++) {
          ndla_h5p_process_params_field($field->field, $params[$i]);
        }
      }
      break;

    case 'text':
      // Update link URLs for node links
      $params = preg_replace_callback('/(<a[^>]*data-ndla-nid=")(\d+)("[^>]*href=")(.*?)("[^>]*>)(.*?)(<\/a>)/',
        function ($matches) {
          $node = db_fetch_object(db_query("SELECT n.nid, n.type, f.field_url_url AS url, f.field_url_title AS title FROM {node} n LEFT JOIN {content_field_url} f ON f.vid = n.vid WHERE n.nid = %d LIMIT 1", $matches[2]));
          if (!$node) {
            return $matches[0]; // Did not find this node
          }
          elseif ($node->type === 'lenke' && !empty($node->url)) {
            if (!empty($node->title) && $matches[6] === '/node/' . $node->nid) {
              $matches[6] = $node->title; // Replace default title with link title
            }
            return $matches[1] . $matches[2] . $matches[3] . $node->url . $matches[5] . $matches[6] . $matches[7];
          }
          else {
            return $matches[1] . $matches[2] . $matches[3] . url('node/' . $node->nid) . $matches[5] . $matches[6] . $matches[7];
          }
        }, $params);
      break;
  }

  return FALSE;
}

/**
 * Callback that outputs node data for the given type.
 *
 * @param Integer $node_id
 */
function ndla_h5p_node_data_callback($node_id) {
  drupal_set_header('Content-type: application/json');
  print json_encode(ndla_h5p_get_node_data($node_id));
}

/**
 * Get node data for usage in h5p library params.
 *
 * @param Integer $node_id
 * @return \stdClass
 */
function ndla_h5p_get_node_data($node_id) {
  $result = new stdClass();
  $baseUrl = (empty($_SERVER['HTTPS']) ? 'http://' : 'https://') . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . base_path();
  $node = db_fetch_object(db_query("SELECT n.title, n.nid AS id, n.vid AS version_id, n.type, cl.license FROM {node} n LEFT JOIN {creativecommons_lite} cl ON cl.nid = n.nid WHERE n.nid = %d", $node_id));

  if ($node) {
    $result->copyright = new stdClass();

    // Copyright title
    $result->copyright->title = $node->title;

    // Copyright authors
    $result->copyright->author = '';
    $authors_result = db_query("SELECT n.title FROM {ndla_authors} na JOIN {node} n ON n.nid = na.person_nid WHERE na.vid = %d", $node->version_id);
    while ($author = db_result($authors_result)) {
      $result->copyright->author .= ($result->copyright->author ? ', ' : '') . $author;
    }

    $result->copyright->year = '';

    // Copyright license
    $result->copyright->license = '';
    if (isset($node->license)) {
      switch ($node->license) {
        case 'gnu':
          $result->copyright->license = 'GNU GPL';
          break;

        case 'by':
          $result->copyright->license = 'CC BY';
          break;

        case 'by-sa':
          $result->copyright->license = 'CC BY-SA';
          break;

        case 'by-nc':
          $result->copyright->license = 'CC BY-NC';
          break;

        case 'by-nc-sa':
          $result->copyright->license = 'CC BY-NC-SA';
          break;

        case 'by-nc-nd':
          $result->copyright->license = 'CC BY-NC-ND';
          break;

        case 'by-nd':
          $result->copyright->license = 'CC BY-ND';
          break;

        case 'publicdomain':
          $result->copyright->license = 'PD';
          break;

        case 'copyrighted':
          $result->copyright->license = 'C';
          break;

        case 'nolaw':
          $result->copyright->license = 'ODC PDDL';
          break;

        case 'noc':
          $result->copyright->license = 'CC PDM';
          break;
      }
    }

    switch ($node->type) {
      case 'image':
        $result->data = db_fetch_object(db_query("SELECT f.filepath as path, f.filemime as mime FROM {image} i JOIN {files} f ON i.fid = f.fid WHERE i.nid = %d AND i.image_size = '_original'", $node_id));
        if ($result->data === FALSE) {
          $result->error = 'No image data found.';
          break;
        }

        $size = @getimagesize($result->data->path);
        if ($size) {
          $result->data->width = $size[0];
          $result->data->height = $size[1];
        }

        $result->data->path = $baseUrl . $result->data->path;
        $result->data->nodeId = $node->id;

        // Copyright source
        $result->copyright->source = db_result(db_query("SELECT field_url_url FROM {content_field_url} WHERE vid = %d", $node->version_id));
        if (!$result->copyright->source) {
          $result->copyright->source = '';
        }
        break;

      case 'video':
        $video_id = db_result(db_query("SELECT iv.videoid FROM {istribute_videos} iv WHERE iv.vid = %d", $node->version_id));
        if (!$video_id) {
          $result->error = 'No video data found.';
          break;
        }

        // Get JSON data
        $curl = curl_init('http://' . variable_get('istribute_endpoint', 'api.istribute.com') . '/video/' . variable_get('istribute_appid', 0) . '/' . $video_id . '.json');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($curl, CURLOPT_TIMEOUT, 5);
        $json = curl_exec($curl);
        curl_close($curl);
        $videodatas = json_decode($json);

        if ($videodatas === FALSE || count($videodatas->sources) === 0) {
          $result->error = 'No video sources found.';
          break;
        }

        $result->data = array();
        $labelToLevel = array(
          'Low' => 0,
          'Medium' => 1,
          'High' => 2,
          'Ultra High' => 3,
        );

        foreach ($videodatas->sources as $source) {
          if ($source->method === 'http') { // alt: preg_match('/^http:\/\/.+\.(mp4|webm)$/i')
            $result->data[] = (object) array(
              'path' => $source->src,
              'mime' => $source->type,
              'codecs' => $source->codecs,
              'quality' => (object) array(
                'level' => $labelToLevel[$source->label],
                'label' => $source->label,
              ),
              'nodeId' => $node->id,
            );
          }
        }

        // Copyright source
        $result->copyright->source = '';
        break;

      case 'audio':
        $audio = db_fetch_object(db_query("SELECT a.format, f.filepath FROM {audio} a JOIN {files} f ON f.fid = a.fid WHERE a.vid = %d", $node->version_id));
        if (!$audio) {
          $result->error = 'No audio data found.';
          break;
        }

        if ($audio->format === 'riff') {
          $audio->format = 'wav';
        }

        $result->data = array(
          (object) array(
            'path' => $baseUrl . $audio->filepath,
            'mime' => 'audio/' . $audio->format,
            'nodeId' => $node->id,
          )
        );

        // Copyright source
        $result->copyright->source = '';
        break;
    }
  }

  if (!isset($result->data)) {
    $result->error = 'Node ' . $node_id . ': Invalid type/data.';
  }

  return $result;
}

/**
 * Implementation of hook_h5p_semantics_alter().
 *
 * Checks for elements of type=text and widget=html. If so, appends
 * mathml tags to tags.
 */
function ndla_h5p_h5p_semantics_alter(&$semantics) {
  foreach ($semantics as $field) {
    // Lists specify the field inside the list.
    while ($field->type == 'list') {
      $field = $field->field;
    }

    if ($field->type == 'group') {
      // Recurse for group.
      ndla_h5p_h5p_semantics_alter($field->fields);
    }
    elseif ($field->type == 'text' && $field->widget == 'html') {

      // Add MathML tags necessary for the NDLA MathML extension to HTML text
      // widget.
      if (!isset($field->tags)) {
        $field->tags = array();
      }
      $field->tags = array_merge($field->tags, array(
        'span',
        'math',
        'maction',
        'maligngroup',
        'malignmark',
        'menclose',
        'merror',
        'mfenced',
        'mfrac',
        'mglyph',
        'mi',
        'mlabeledtr',
        'mlongdiv',
        'mmultiscripts',
        'mn',
        'mo',
        'mover',
        'mpadded',
        'mphantom',
        'mroot',
        'mrow',
        'ms',
        'mscarries',
        'mscarry',
        'msgroup',
        'msline',
        'mspace',
        'msqrt',
        'msrow',
        'mstack',
        'mstyle',
        'msub',
        'msup',
        'msubsup',
        'mtable',
        'mtd',
        'mtext',
        'mtr',
        'munder',
        'munderover',
        'semantics',
        'annotation',
        'annotation-xml',
      ));

      // Add extra font-size options to all CKEditor fields
      $field->font->size = array(
        (object) array(
          'label' => '50%',
          'css' => '0.5em'
        ),
        (object) array(
          'label' => '56.25%',
          'css' => '0.5625em'
        ),
        (object) array(
          'label' => '62.50%',
          'css' => '0.625em'
        ),
        (object) array(
          'label' => '68.75%',
          'css' => '0.6875em'
        ),
        (object) array(
          'label' => '75%',
          'css' => '0.75em'
        ),
        (object) array(
          'label' => '87.50%',
          'css' => '0.875em'
        ),
        (object) array(
          'label' => '100%',
          'css' => '1em'
        ),
        (object) array(
          'label' => '112.50%',
          'css' => '1.125em'
        ),
        (object) array(
          'label' => '125%',
          'css' => '1.25em'
        ),
        (object) array(
          'label' => '137.50%',
          'css' => '1.375em'
        ),
        (object) array(
          'label' => '150%',
          'css' => '1.5em'
        ),
        (object) array(
          'label' => '162.50%',
          'css' => '1.625em'
        ),
        (object) array(
          'label' => '175%',
          'css' => '1.75em'
        ),
        (object) array(
          'label' => '225%',
          'css' => '2.25em'
        ),
        (object) array(
          'label' => '300%',
          'css' => '3em'
        ),
        (object) array(
          'label' => '450%',
          'css' => '4.5em'
        ),
        (object) array(
          'label' => '675%',
          'css' => '6.75em'
        ),
        (object) array(
          'label' => '1350%',
          'css' => '13.5em'
        ),
        (object) array(
          'label' => '3375%',
          'css' => '33.75em'
        )
      );
    }
    else if ($field->type == 'image' || $field->type == 'video' || $field->type == 'audio' || $field->type == 'library') {
      if (!isset($field->extraAttributes)) {
        $field->extraAttributes = array('nodeId');
      }
      elseif (!in_array('nodeId', $field->extraAttributes)) {
        $field->extraAttributes[] = 'nodeId';
      }
    }
  }
}

function ndla_h5p_get_node_params_callback($node_id, $link = FALSE) {
  $node = node_load($node_id);

  $result = array(
    'title' => $node->title
  );
  if ($link) {
    if ($node->type === 'lenke' && isset($node->field_url) && isset($node->field_url[0]) && isset($node->field_url[0]['url']) && trim($node->field_url[0]['url']) !== '') {
      $url = $node->field_url[0]['url'];
      if (isset($node->field_url[0]['title']) && $node->field_url[0]['title'] !== '') {
        $title = $node->field_url[0]['title'];
      }
    }
    else {
      $url = url('node/' . $node_id);
    }
    if (!isset($title)) {
      $title = $node->title;
    }
    $result['library'] = 'H5P.Link 1.0';
    $result['params'] = json_encode(array(
      'title' => $title,
      'url' => $url,
    ));
  }
  elseif (isset($node->main_library)) {
    $result['library'] = (isset($node->main_library['name']) ? $node->main_library['name'] : $node->main_library['machineName']) . ' ' . $node->main_library['majorVersion'] . '.' . $node->main_library['minorVersion'];
    $result['params'] = $node->json_content;
  }

  drupal_set_header('Content-type: application/json');
  print json_encode($result);
}
