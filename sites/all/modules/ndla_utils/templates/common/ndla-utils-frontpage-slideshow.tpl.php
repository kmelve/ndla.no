<div class='slideshow-feeds'>
  <?php for ($i = 0, $size = count($new_content); $i < $size; $i++): ?>
    <?php if($new_content[$i]->context != 0) {
      $url = url('node/' . $new_content[$i]->nid, array('query' => 'fag=' . $new_content[$i]->context));
      } else {
        $url = url('node/' . $new_content[$i]->nid);
      }?>
  <div<?php print $i == 0 ? ' class="first"' : '' ?>>
    <div class="new-content-image-wrapper">
      <a href="<?php print url('node/' . $new_content[$i]->nid) ?>">
        <img src="<?php print $base_path . ($new_content[$i]->image ? $new_content[$i]->image : $directory . '/img/placeholder.png') ?>" alt=""/>
      </a>


      <a href="<?php print url('node/135085') ?>" class="actuality" title="<?php print t('See more') ?>"><?php print t('See more') ?></a>

    <div class='front-slideshow-nav'></div>
    </div>
    <div class='text_container'>
      <h3><a href="<?php print $url?>"><?php print check_plain($new_content[$i]->title) ?></a></h3>
      <p class="new-content">
      <?php
            $char_count_allowed = 300;
            $content = preg_replace('/\[.*\]/', '', strip_tags(preg_replace('/<br \/>|<\/p>/', ' ', $new_content[$i]->teaser)));
            if(strlen($content) > $char_count_allowed) {
                $content = node_teaser($content,NULL, $char_count_allowed) . "...";
            }
            print $content;
      ?>
      </p>
    </div>
  </div>
  <?php endfor; ?>
</div>
<?php if(count($new_content) > 1): ?>
  <div class='play-pause'>
    <a class='play' href='javascript:void(0)'><img alt='Play' src="/sites/all/themes/ndla2010/img/slideshow_play.png" alt="" title="" width="20" height="20"></a>
    <a class='pause' href='javascript:void(0)'><img alt='Pause' src="/sites/all/themes/ndla2010/img/slideshow_pause.png" alt="" title="" width="20" height="20"></a>
  </div>
<?php endif; ?>
