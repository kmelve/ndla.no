<?php
/**
 * @file
 * @ingroup ndla_utils
 * @brief
 *  Contains all the functions for handling the menu callback at admin/content/node
 */

define('HAS_TRANSLATION', 1);
define('HAS_NOT_TRANSLATION', 2);
define('TIME_FORMAT', 'd.m.Y');

function ndla_utils_red_node_search_form() {
  global $user;
  $resp_click = '<a href="javascript:void(0);" onclick="$(\'#responsible_user\').val(\'' . $user->name . '\');">' . t('Me') . '</a>';
  $user_click = '<a href="javascript:void(0);" onclick="$(\'#user_create\').val(\'' . $user->name . '\');">' . t('Me') . '</a>';

  $notify = '</table>';
  if(module_exists('ndla_notify')) {
    $notify = '';
  }
  if(isset($_GET['op']) && $_GET['op'] == t('Clear')) {
    $path = $_GET['q'];
    drupal_goto($path);
  }

  drupal_set_title('Search content');
  $form = array('#method' => 'get');
  $filters = _ndla_utils_get_enabled_filters();
  $options = _ndla_utils_red_get_search_options();
  $default_dates = array(
    'date_created' => '',
    'date_created_to' => '',
    'date_time_limit' => '',
    'date_time_limit_to' => '',
    'date_to_publish_queue' => '',
    'date_to_publish_queue_to' => '',
    'date_published' => '',
    'date_published_to' => '',
    'date_rejected' => '',
    'date_rejected_to' => '',
  );

  foreach($default_dates as $name => $dummy) {
    if(isset($filters[$name]) && !empty($filters[$name])) {
      $d = explode(".", $filters[$name]);
      $d = array_reverse($d);
      $default_dates[$name] = implode("-", $d);
    }
  }
  
  $statuses = array('' => t('All')) + ndla_publish_workflow_get_states();
  $form['basic'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search'),
    '#collapsed' => FALSE,
    '#collapsible' => TRUE,
    
    'save-search-name' => array(
      '#type' => 'hidden',
      '#default_value' => isset($_GET['save-search-name']) ? check_plain($_GET['save-search-name']) : '',
      '#size' => 35,
    ),
    
    'node_title' => array(
      '#prefix' => '<table class="ndla_utils_node_search"><tr><td>',
      '#suffix' => '</td></tr>',
      '#title' => t('Title'),
      '#type' => 'textfield',
      '#default_value' => $filters['node_title'],
      '#size' => 35,
    ),
    
    'user' => array(
      '#id' => 'user_create',
      '#prefix' => '<tr><td>',
      '#suffix' => $user_click . '</td>',
      '#title' => t('User'),
      '#type' => 'textfield',
      '#autocomplete_path' => 'content/user/autocomplete',
      '#default_value' => $filters['user'],
      '#size' => 35,
    ),
    
    'responsible_user' => array(
      '#id' => 'responsible_user',
      '#prefix' => '<td>',
      '#suffix' => $resp_click  . '</td>',
      '#title' => t('Responsible'),
      '#type' => 'textfield',
      '#autocomplete_path' => 'content/user/autocomplete',
      '#default_value' => $filters['responsible_user'],
      '#size' => 35,
    ),
    
    'translated' => array(
      '#prefix' => '</tr><tr><td>',
      '#suffix' => '</td>',
      '#title' => t('Translation status'),
      '#type' => 'select',
      '#options' => array('' => t('All'), HAS_TRANSLATION => t('Has translation'), HAS_NOT_TRANSLATION => t('Has not translation')),
      '#default_value' => $filters['translated'],
    ),

    'pub_queue' => array(
      '#prefix' => '<td>',
      '#suffix' => '</td></tr></table>',
      '#title' => t('Status'),
      '#options' => $statuses,
      '#type' => 'select',
      '#multiple' => FALSE,
      '#default_value' => $filters['pub_queue'],
    ),
    
    'owned_by_group' => array(
      '#prefix' => '<table class="ndla_utils_node_search"><tr><td>',
      '#suffix' => '</td>',
      '#title' => t('Owned by subject'),
      '#type' => 'select',
      '#options' => $options['groups'],
      '#multiple' => FALSE,
      '#default_value' => $filters['owned_by_group'],
    ),
    'content_type' => array(
      '#prefix' => '<td>',
      '#suffix' => '</td></tr>',
      '#title' => t('Content type'),
      '#type' => 'select',
      '#options' => $options['content_type'],
      '#multiple' => FALSE,
      '#default_value' => $filters['content_type'],
    ),
    'belongs_to_group' => array(
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
      '#title' => t('Used by subject'),
      '#type' => 'select',
      '#options' => $options['groups'],
      '#multiple' => FALSE,
      '#default_value' => $filters['belongs_to_group'],
    ),
    'node_type' => array(
      '#prefix' => '<td>',
      '#suffix' => '</td></tr>',
      '#title' => t('Node type'),
      '#type' => 'select',
      '#options' => $options['node_type'],
      '#multiple' => FALSE,
      '#default_value' => $filters['node_type'],
    ),
    'date_created' => array( //From date
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
      '#title' => t('Date created (from)'),
      '#type' => 'date_popup',
      '#date_format' => TIME_FORMAT,
      '#date_year_range' => '-10:0',
      '#default_value' => $default_dates['date_created'],
    ),
    'date_created_to' => array( //To date
      '#prefix' => '<td>',
      '#suffix' => '</td></tr>',
      '#title' => t('Date created (to)'),
      '#type' => 'date_popup',
      '#date_format' => TIME_FORMAT,
      '#date_year_range' => '-10:0',
      '#default_value' => $default_dates['date_created_to'],
    ),
    'node_language' => array(
      '#prefix' => '<tr><td>',      
      '#suffix' => '</td>',
      '#title' => t('Language'),
      '#type' => 'select',
      '#options' => $options['lang_options'],
      '#multiple' => FALSE,
      '#default_value' => $filters['node_language'],
    ),
    'scanpix_id' => array(
      '#prefix' => '<td>',
      '#suffix' => '</td></tr>',
      '#title' => t('Scanpix ID'),
      '#type' => 'textfield',
      '#default_value' => $filters['scanpix_id'],
      '#size' => 35,
    ),
    'editorial_keywords' => array(
      '#prefix' => '<tr><td>',
      '#suffix' => '</td></tr>',
      '#title' => t('Editorial Keywords'),
      '#type' => 'textfield',
      '#default_value' => $filters['editorial_keywords'],
      '#size' => 35,
    ),
    'date_time_limit' => array( //From date
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
      '#title' => t('Agreement Expiration date (from)'),
      '#type' => 'date_popup',
      '#date_format' => TIME_FORMAT,
      '#date_year_range' => '-2:+5',
      '#default_value' => $default_dates['date_time_limit'],
    ),
    'date_time_limit_to' => array( //To date
      '#prefix' => '<td>',
      '#suffix' => '</td></tr>' . $notify,
      '#title' => t('Agreement Expiration date (to)'),
      '#type' => 'date_popup',
      '#date_format' => TIME_FORMAT,
      '#date_year_range' => '-2:+5',
      '#default_value' => $default_dates['date_time_limit_to'],
    ),
    
    'limit' => array(
      '#title' => t('Number of results'),
      '#type' => 'select',
      '#default_value' => $filters['limit'],
      '#options' => $options['limit'],
      '#prefix' => '<tr><td>',
      '#suffix' => '</td></tr>',
    ),
  );
  
  if(empty($notify)) {
    $form['basic']['notify'] = array(
      '#title' => t('Nodes which needs to be updated'),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td></tr></table>',
      '#default_value' => $filters['notify']  ,
      '#type' => 'checkbox');
  }

  if(in_array('desk', $user->roles) || $user->uid == 1) {
    $form['desk'] = array(
      '#type' => 'fieldset',
      '#title' => t('Desk'),
      '#collapsed' => FALSE,
      '#collapsible' => TRUE,
      'table-start' => array(
        '#type' => 'markup',
        '#value' => '<table class="ndla_utils_node_search">',
      ),
      'date_to_publish_queue' => array(
        '#prefix' => '<tr><td>',
        '#suffix' => '</td>',
        '#title' => t('Sent to publish queue (from)'),
        '#type' => 'date_popup',
        '#date_format' => TIME_FORMAT,
        '#date_year_range' => '-2:+5',
        '#default_value' => $default_dates['date_to_publish_queue'],
      ),
      'date_to_publish_queue_to' => array(
        '#prefix' => '<td>',
        '#suffix' => '</td></tr>' . $notify,
        '#title' => t('Sent to publish queue (to)'),
        '#type' => 'date_popup',
        '#date_format' => TIME_FORMAT,
        '#date_year_range' => '-2:+5',
        '#default_value' => $default_dates['date_to_publish_queue_to'],
      ),
      'first_time_published' => array(
        '#prefix' => '<tr><td>',
        '#suffix' => '</td></tr>',
        '#title' => t('First time published.'),
        '#type' => 'checkbox',
        '#default_value' => $filters['first_time_published'],
      ),
      'date_published' => array(
        '#prefix' => '<tr><td>',
        '#suffix' => '</td>',
        '#title' => t('Published (from)'),
        '#type' => 'date_popup',
        '#date_format' => TIME_FORMAT,
        '#date_year_range' => '-2:+5',
        '#default_value' => $default_dates['date_published'],
      ),
      'date_published_to' => array(
        '#prefix' => '<td>',
        '#suffix' => '</td></tr>' . $notify,
        '#title' => t('Published (to)'),
        '#type' => 'date_popup',
        '#date_format' => TIME_FORMAT,
        '#date_year_range' => '-2:+5',
        '#default_value' => $default_dates['date_published_to'],
      ),
      'date_rejected' => array(
        '#prefix' => '<tr><td>',
        '#suffix' => '</td>',
        '#title' => t('To clarify (from)'),
        '#type' => 'date_popup',
        '#date_format' => TIME_FORMAT,
        '#date_year_range' => '-2:+5',
        '#default_value' => $default_dates['date_rejected'],
      ),
      'date_rejected_to' => array(
        '#prefix' => '<td>',
        '#suffix' => '</td></tr>' . $notify,
        '#title' => t('To clarify (to)'),
        '#type' => 'date_popup',
        '#date_format' => TIME_FORMAT,
        '#date_year_range' => '-2:+5',
        '#default_value' => $default_dates['date_rejected_to'],
      ),
      'table-end' => array(
        '#type' => 'markup',
        '#value' => '</table>',
      ),
    );
  }
  
  $form['buttons'] = array(
    'search' => array(
      '#type' => 'submit',
      '#value' => t('Search')
    ),
    'clear' => array(
      '#type' => 'submit',
      '#value' => t('Clear'),
    ),
  );

  return $form;
}

function ndla_utils_red_get_search_results_block() {
  if(isset($_GET['op'])) {
    $form = array();
    $form['#theme'] = "ndla_utils_red_node_search_form";
    $filters = _ndla_utils_get_enabled_filters(TRUE);
    $items = _ndla_utils_red_get_searched_items($filters);
    drupal_add_js(drupal_get_path('module', 'ndla_utils') . '/js/ndla_utils_red.search.js?' . rand());
    drupal_add_css(drupal_get_path('module', 'ndla_utils') . '/css/ndla_utils_red.search.css', 'module');
    $nids = array();
    if(count($items)) {
      foreach($items as $node) {
        $nids[$node['nid']] = '';
        $form['nodes'][$node['nid']]['title'] = array(
          '#value' => $node['title'] . (empty($node['thumbnail']) ? '' : '<div class="thumbnail"><img src="/' . $node['thumbnail'] . '"></div>'),
        );
        $form['nodes'][$node['nid']]['nodeid'] = array(
          '#value' => $node['nid'],
        );
        $form['nodes'][$node['nid']]['type'] = array(
          '#value' => $node['type'],
        );
        $form['nodes'][$node['nid']]['language'] = array(
          '#value' => $node['language'],
        );
        $form['nodes'][$node['nid']]['owned_by_group'] = array(
          '#value' => $node['owned_by_group'],
        );
        $form['nodes'][$node['nid']]['used_by_group'] = array(
          '#value' => $node['used_by_group'],
        );
        $form['nodes'][$node['nid']]['created'] = array(
          '#value' => $node['created'],
        );
        $form['nodes'][$node['nid']]['changed'] = array(
          '#value' => $node['changed'],
        );
        $form['nodes'][$node['nid']]['translated'] = array(
          '#value' => $node['translated'],
        );
        $form['nodes'][$node['nid']]['user'] = array(
          '#value' => $node['user'],
        );
        $form['nodes'][$node['nid']]['workflow_status'] = array(
          '#value' => $node['workflow_status'],
        );
      }
      $form['checkboxes']= array(
        '#type' => 'checkboxes',
        '#options' => $nids,
      );
    }
  
    return $form;
  }
}

function _ndla_utils_get_enabled_filters($escape = FALSE) {
  $filters = array(
    'node_title' => '',
    'user' => '',
    'responsible_user' => '',
    'translated' => '',
    'pub_queue' => '',
    'owned_by_group' => '',
    'belongs_to_group' => '',
    'content_type' => '',
    'node_type' => '',
    'date_created' => '',
    'date_created_to' => '',
    'node_language' => '',
    'scanpix_id' => '',
    'editorial_keywords' => '',
    'notify' => '',
    'date_time_limit' => '',
    'date_time_limit_to' => '',
    'limit' => '',
    'date_to_publish_queue' => '',
    'date_to_publish_queue_to' => '',
    'first_time_published' => '',
    'date_published' => '',
    'date_published_to' => '',
    'date_rejected' => '',
    'date_rejected_to' => '',
  );
  
  foreach($filters as $filter => $dummy) {
    $value = '';
    if(isset($_REQUEST[$filter])) {
      if(strpos($filter, 'date') !== FALSE) {
        $value = $_REQUEST[$filter]['date'];
      }
      else if($filter == 'limit') {
        if(!is_numeric($_REQUEST[$filter])) {
          $value = 25;
        }
        else {
          $value = ($_REQUEST[$filter] > 100 || $_REQUEST[$filter] < 1) ? 25 : $_REQUEST[$filter];
        }
      }
      else {
        $value = $_REQUEST[$filter];
      }
      if($escape) {
        $value = addslashes($value);
      }
      $filters[$filter] = trim($value);
    }
  }

  return $filters;
}

function _ndla_utils_red_get_search_options() {
  $options = array();
  $options['node_type'][''] = t('All');
  $options['lang_options'] = array('' => t('All'), 'neutral' => t('Neutral'), );
  
  foreach (language_list() as $lang) {
    $options['lang_options'][$lang->language] = $lang->name;
  }
  
  foreach (node_get_types() as $type) {
    $options['node_type'][$type->type] = $type->name;
  }
  
  $groups = ndla_fag_taxonomy_get_vocabulary_leafs();
  $options['groups'][] = t('All');
  foreach($groups as $key => $group) {
    $options['groups'][$group->tid] = $group->name;
  }
  $options['content_type'] = _ndla_utils_red_get_content_type_options();
  
  $options['limit'] = array(25 => 25, 50 => 50, 100 => 100);
  return $options;
}

function _ndla_utils_red_get_group_options(&$options = array(), $parent = NULL, &$level = 0) {
  if(!$parent) {
    $parent = ndla_fag_taxonomy_get_vocabulary_tree();
    $parent = $parent->terms;
    $options[''] = t('All');
    _ndla_utils_red_get_group_options($options, $parent, $level);
  }
  
  foreach($parent as $term) {
    $suffix = ($level > 0) ? implode("", array_fill(0, $level , "-")) : "";
    $level_before = $level;
    $options[$term->tid] = trim($suffix . " " . $term->name);
    
    if(is_array($term->terms)) {
      $level += 1;
      _ndla_utils_red_get_group_options($options, $term->terms, $level);
    }
    $level = $level_before;
  }
  
  return $options;
}

function _ndla_utils_red_get_content_type_options() {
  $ct_vid = ndla_utils_get_content_type_vocab();
  $tree = taxonomy_get_tree($ct_vid);
  $options = array('' => t('All'));
  if ($tree && (count($tree) > 0)) {
    foreach ($tree as $term) {
      $options[$term->tid] = trim(str_repeat('-', $term->depth) . " " . $term->name);
    }
  }
  
  return $options;
}

function _ndla_utils_red_get_searched_items($filters) {
  $nodes = $where = $joined_tables = array();
  $joins = array("INNER JOIN {node_revisions} r ON n.nid=r.nid AND n.vid=r.vid");
  $joins[] = "INNER JOIN {users} u ON u.uid = r.uid";

  $fields = array("n.title, n.language, n.nid, n.vid, n.tnid, n.type, n.created, n.changed, n.uid");
  $sql = '';
  
  
  if($filters['node_title']) {
    $where[] = "n.title LIKE '%%" . $filters['node_title'] . "%%'";
  }
 
  if($filters['user']) {
    $where[] = "u.name = '" . $filters['user'] . "'";
  }
  
  if($filters['translated']) {
    if($filters['translated'] == HAS_TRANSLATION) {
      $where[] = "n.tnid <> ''";
    }
    else {
      $where[] = "n.tnid = ''";
    }
  }
  
  if($filters['notify']) {
    $where[] = "notify.notify_date <= " . time();
    $joins[] = "INNER JOIN {ndla_notify} notify ON notify.nid = n.nid";
  }
  
  if($filters['node_type']) {
    $where[] = "n.type = '" . $filters['node_type'] . "'";
  }
  
  if($filters['date_created']) {
    $filters['date_created'] .= " 00:00:00";
    $time = strtotime($filters['date_created']);
    if($time) {
      $where[] = "n.created >= " . $time;
    }
  }
  if($filters['date_created_to']) {
    $filters['date_created_to'] .= " 23:59:59";
    $time = strtotime($filters['date_created_to']);
    if($time) {
      $where[] = "n.created <= " . $time;
    }
  }
  
  if($filters['node_language']) {
    $where[] = "n.language = '" . (($filters['node_language'] != 'neutral') ? $filters['node_language'] : "") . "'";
  }
  
  if($filters['scanpix_id']) {
    $joins[] = "INNER JOIN {ndla_scanpix} ns ON (n.nid = ns.nid)";
    $where[] = "ns.scanpix_id = '" . $filters['scanpix_id'] . "'";
  }
  
  if($filters['editorial_keywords']) {
    $editorial_keywords = explode(",", $filters['editorial_keywords']);
    $joins[] = "INNER JOIN {term_node} tn ON (n.nid = tn.nid AND n.vid = tn.vid) INNER JOIN {term_data} td ON (tn.tid = td.tid) INNER JOIN {vocabulary} v ON (td.vid = v.vid)";
    $where[] = "td.name IN ('" . implode("', '", $editorial_keywords) . "') AND v.name = 'Editorial Keywords'";
  }
  
  if($filters['date_time_limit']) {
    $filters['date_time_limit'] .= " 00:00:00";
    $time = strtotime($filters['date_time_limit']);
    if(empty($joined_tables['content_type_kontrakt']))
        $joins[] = "INNER JOIN {content_type_kontrakt} ctk ON (n.nid = ctk.nid AND n.vid = ctk.vid)";
    $where[] = "ctk.field_time_limit_value2 >= '" . $time . "'";
    $joined_tables['content_type_kontrakt'] = 1;
  }
  
  if($filters['date_time_limit_to']) {
    $filters['date_time_limit_to'] .= " 23:59:59";
    $time = strtotime($filters['date_time_limit_to']);
    if(empty($joined_tables['content_type_kontrakt']))
        $joins[] = "INNER JOIN {content_type_kontrakt} ctk ON (n.nid = ctk.nid AND n.vid = ctk.vid)";
    $where[] = "ctk.field_time_limit_value2 <= '" . $time . "'";
    $joined_tables['content_type_kontrakt'] = 1;
  }
  
  if($filters['pub_queue'] || $filters['pub_queue'] == '0') {
    $joins[] = "INNER JOIN {ndla_publish_workflow_status} s1 ON (s1.nid=n.nid AND s1.vid=n.vid)";
    $where[] = "s1.status= " . $filters['pub_queue'];
    $where[] = "s1.timestamp=(SELECT MAX(s2.timestamp) FROM ndla_publish_workflow_status s2 WHERE s2.nid=s1.nid)";
    $joined_tables['ndla_publish_workflow_status'] = 1;
  }
  
  if($filters['responsible_user']) {
    //Fetch the uid
    $uid = db_fetch_object(db_query("SELECT uid FROM {users} WHERE name = '%s'", $filters['responsible_user']))->uid;
    if(!isset($joined_tables['ndla_publish_workflow_status'])) {
      $joins[] = "INNER JOIN {ndla_publish_workflow_status} s1 ON (s1.nid=n.nid AND s1.vid=n.vid)";
    }
    if(!$uid) {
      $uid = -1;
    }
    $where[] = "s1.responsible_uid = " . $uid;
  }
  
  if($filters['owned_by_group']) {
    $row = db_fetch_object(db_query("SELECT nid FROM {ndla_fag_taxonomy_map} WHERE tid = %d", $filters['owned_by_group']));
    $joins[] = "INNER JOIN {og_ancestry} oga ON oga.nid = n.nid";
    $where[] = "oga.group_nid = " . $row->nid;
    $joined_tables['og_ancestry'] = 1;
  }
  
  if($filters['content_type']) {
    $taxonomy_vid = ndla_utils_get_content_type_vocab();
    $joined_tables['term_node'] = 1;
    $joined_tables['term_data'] = 1;
    $joins[] = "INNER JOIN {term_node} tn ON tn.nid = n.nid AND tn.vid = r.vid";
    $joins[] = "INNER JOIN {term_data} td2 ON tn.tid = td2.tid";
    $where[] = "td2.tid = " . $filters['content_type'];
  }
  
  //Taxonomy group thing
  if($filters['belongs_to_group']) {
    if(!isset($joined_tables['term_node'])) {
      $joins[] = "INNER JOIN {term_node} tn ON tn.nid = n.nid AND tn.vid = r.vid";
    }
    $joins[] = "INNER JOIN {term_data} td ON td.tid = tn.tid";
    $joins[] = "INNER JOIN {ndla_fag_taxonomy_map} nftm ON nftm.tid = td.tid";
    $where[] = "nftm.tid = " . $filters['belongs_to_group'];
  }

  //Dont join the organic groups tables unless we really need to
  if(isset($_REQUEST['order']) && $_REQUEST['order'] == t('Owned by')) {
    if(!isset($joined_tables['og_ancestry'])) {
      $joins[] = "LEFT JOIN {og_ancestry} oga ON oga.nid = n.nid";
    }
    $joins[] = "LEFT JOIN {node} group_table ON oga.group_nid = group_table.nid";
    $joins[] = "LEFT JOIN {ndla_fag_taxonomy_map} nftm2 ON nftm2.nid = group_table.nid";
    $joins[] = "LEFT JOIN {term_data} td_tax2 ON td_tax2.tid = nftm2.tid";
  }
  else if(isset($_REQUEST['order']) && $_REQUEST['order'] == t('Used by')) {
    $vocabulary = ndla_fag_taxonomy_get_default_vocabulary();
    if(!$joined_tables['term_node']) {
      $joins[] = "LEFT JOIN {term_node} tn ON r.vid = tn.vid AND r.nid = tn.nid";
    }
    $joins[] = "LEFT JOIN {term_data} term_group ON tn.tid = term_group.tid AND term_group.vid = " . $vocabulary->vid;
    $joins[] = "LEFT JOIN {ndla_fag_taxonomy_map} nftm ON nftm.tid = term_group.tid";
    $joins[] = "LEFT JOIN {node} used_by ON used_by.nid = nftm.nid AND used_by.type='fag'";
  }

  $to = '';
  if(!empty($filters['date_to_publish_queue']) || !empty($filters['date_to_publish_queue_to'])) {
    $joins[] = "INNER JOIN {ndla_publish_workflow_status} dtpq ON (n.nid=dtpq.nid AND n.vid=dtpq.vid)";
    if(!empty($filters['date_to_publish_queue'])) {
      $time = strtotime($filters['date_to_publish_queue'] . ' 00:00:00');
      $where[] = "dtpq.timestamp >= '" . $time . "'";
    }
    if(!empty($filters['date_to_publish_queue_to'])) {
      $time = strtotime($filters['date_to_publish_queue_to'] . ' 23:59:59');
      $where[] = "dtpq.timestamp <= '" . $time . "'";
      $to = "timestamp <= '" . $time . "' AND";
    }
    $where[] = "dtpq.status = '2'";
  }
  if(!empty($filters['first_time_published'])) {
    $where[] = "n.nid IN(SELECT nid FROM ndla_publish_workflow_status WHERE $to status = '2' GROUP BY nid HAVING count(*) = 1)";
  }

  if(!empty($filters['date_published']) || !empty($filters['date_published_to'])) {
    $joins[] = "INNER JOIN {ndla_publish_workflow_status} dp ON (n.nid=dp.nid AND n.vid=dp.vid)";
    if(!empty($filters['date_published'])) {
      $time = strtotime($filters['date_published'] . ' 00:00:00');
      $where[] = "dp.timestamp >= '" . $time . "'";
    }
    if(!empty($filters['date_published_to'])) {
      $time = strtotime($filters['date_published_to'] . ' 23:59:59');
      $where[] = "dp.timestamp <= '" . $time . "'";
    }
    $where[] = "dp.status = '3'";
  }

  if(!empty($filters['date_rejected']) || !empty($filters['date_rejected_to'])) {
    $joins[] = "INNER JOIN {ndla_publish_workflow_status} dr ON (n.nid=dr.nid AND n.vid=dr.vid)";
    if(!empty($filters['date_rejected'])) {
      $time = strtotime($filters['date_rejected'] . ' 00:00:00');
      $where[] = "dr.timestamp >= '" . $time . "'";
    }
    if(!empty($filters['date_rejected_to'])) {
      $time = strtotime($filters['date_rejected_to'] . ' 23:59:59');
      $where[] = "dr.timestamp <= '" . $time . "'";
    }
    $where[] = "dr.status = '5'";
  }

  $where = count($where) ? "WHERE " . implode(" AND ", $where) : "";
  $joins = implode("\n", $joins);
  $sql = "SELECT " . implode(", ", $fields) . " FROM {node} n $joins $where " . tablesort_sql(_ndla_utils_red_node_search_header());


  //die(str_replace("{", "", str_replace("}", "", $sql)));
  if(empty($filters['limit'])) {
    $filters['limit'] = 25;
  }
  $result = pager_query($sql, $filters['limit']);

  $lang_list = language_list();
  $lang_list['']->native = t('Neutral');

  $types = node_get_types();
  $states = ndla_publish_workflow_get_states();
  while($row = db_fetch_array($result)) {
    //dont do node_load
    $fake_node->nid = $row['nid'];
    $fake_node->vid = $row['vid'];
    $fags = ndla_fag_taxonomy_get_node_terms($row['nid'], $row['vid']);
    $row['owned_by_group'] = array();
    $owned_by_group = ndla_fag_taxonomy_get_node_group_terms($row['nid'], $row['vid']);
    foreach($owned_by_group as $fag) {
      if($fag->gid) {
        $row['owned_by_group'][] = $fag->name;
      }
    }

    $used_by_group = ndla_fag_taxonomy_get_node_terms($row['nid'], $row['vid']);
    $row['used_by_group'] = array();
    foreach($fags as $fag) {
      if($fag->gid) {
        $row['used_by_group'][] = $fag->name;
      }
    }
    
    $row['thumbnail'] = db_fetch_object(db_query('SELECT f.filepath FROM {image} i LEFT JOIN {files} f ON (i.fid = f.fid) WHERE i.image_size = "thumbnail" AND i.nid = %d', $row['nid']))->filepath;

    $row['owned_by_group'] = implode(", ", $row['owned_by_group']);
    $row['used_by_group'] = implode(", ", $row['used_by_group']);
    $row['language'] = $lang_list[$row['language']]->native;
    $row['created'] = date(TIME_FORMAT, $row['created']);
    $row['changed'] = date(TIME_FORMAT, $row['changed']);
    $row['translated'] = is_numeric($row['tnid']) && $row['tnid'] > 0 ? t('Yes') : t('No');
    $row['type'] = $types[$row['type']]->name;
    $row['user'] = ndla_utils_get_username($row['uid']);
    $row['title'] = trim($row['title']) == '' ? t('No title') : $row['title'];
    $row['title'] = l($row['title'], 'node/' . $row['nid']);
    $row['workflow_status'] = $states[ndla_publish_workflow_get_current_state($fake_node)];
    $nodes[$row['nid']] = $row;
  }
  
  return $nodes;
}

/**
 * Both theme_ndla_utils_red_node_search_form and _ndla_utils_red_get_searched_items needs this function.
 * It returns the table header which contains sorting info.
 */
function _ndla_utils_red_node_search_header() {
  $header = array(
    theme('table_select_header_cell'), 
    array('data' => t('Title'), 'field' => 'n.title'),
    array('data' => t('Node id'), 'field' => 'n.nid'),
    array('data' => t('Type'), 'field' => 'n.type'),
    array('data' => t('Owned by'), 'field' => 'td_tax2.name'),
    array('data' => t('Used by'), 'field' => 'used_by.title'),
    array('data' => t('Language'), 'field' => 'n.language'),
    array('data' => t('Created'), 'field' => 'n.created'),
    array('data' => t('Updated'), 'field' => 'n.changed', 'sort' => 'desc'),
    t('Has translation'),
    array('data' => t('Author'), 'field' => 'u.name'),
    t('Status'),
  );
  
  return $header;
}

function theme_ndla_utils_red_node_search_form($form) {
  drupal_add_css(drupal_get_path('module', 'ndla_utils_red') . "/css/ndla_utils_red.css");
  $header = _ndla_utils_red_node_search_header();
  $output = "";
  
  $rows = array();
  if(count($form['nodes'])) {
    foreach (element_children($form['checkboxes']) as $key) {
      $row = array();
      //The node id checkboxes
      $row[] = drupal_render($form['checkboxes'][$key]);
    
      //The node title
      $row[] = drupal_render($form['nodes'][$key]['title']);
    
      //The node id
      $row[] = drupal_render($form['nodes'][$key]['nodeid']);
    
      //The node type
      $row[] = drupal_render($form['nodes'][$key]['type']);
    
      //The node group(s)
      $row[] = drupal_render($form['nodes'][$key]['owned_by_group']);

      //The node group(s)
      $row[] = drupal_render($form['nodes'][$key]['used_by_group']);
    
      //The node language
      $row[] = drupal_render($form['nodes'][$key]['language']);
    
      //The node creation date
      $row[] = drupal_render($form['nodes'][$key]['created']);
    
      //The node update date
      $row[] = drupal_render($form['nodes'][$key]['changed']);
    
      //If the node has a translation
      $row[] = drupal_render($form['nodes'][$key]['translated']);
    
      //If the node has a translation
      $row[] = array('data' => drupal_render($form['nodes'][$key]['user']), 'class' => 'author');
    
      //The workflow status
      $row[] = drupal_render($form['nodes'][$key]['workflow_status']);
    
      $rows[] = $row;
    }
    $output = theme('table', $header, $rows, array('class' => 'ndla_utils_red_search_table'));
  }
  else if(isset($form['nodes'])) {
    $output = "<p>&nbsp;</p><p>" . t('Search returned no results') . "</p>";
  }
  return drupal_render($form) . $output . theme('pager', NULL, 20, 0);
}
