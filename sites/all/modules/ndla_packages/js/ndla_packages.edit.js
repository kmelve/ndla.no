Drupal.behaviors.ndlaPackages = function(context) {
  $('#ndla-package-create').click(function() {
    window.open(Drupal.settings.basePath + 'ndla-packages/edit?goto=' + Drupal.settings.ndla_packages_host + '/packages/create');
  });
  $('#ndla-package-edit').click(function() {
    var url = $('#edit-ndla-packages-url').val();
    if(!url.match(/\/copy$/) && !url.match(/\/translate\//)) {
      url += '/edit';
    }
    window.open(Drupal.settings.basePath + 'ndla-packages/edit?goto=' + url);
  });
  function receiveMessage(event)
  {
    if (event.origin !== Drupal.settings.ndla_packages_host) {
      return;
    }
    //Phase 1
    //$('#edit-ndla-packages-url').val(Drupal.settings.ndla_packages_host + event.data);
    //Phase 2
    $('#edit-title').val(event.data.title);
    $('#edit-ndla-packages-url').val(Drupal.settings.ndla_packages_host + event.data.url);
  }
  window.addEventListener('message', receiveMessage, false);
};