<?php

/**
 * @file
 *  cookbook.xhr.inc php file
 *  Contains XMLHttpRequest functions for the Drupal module cookbook.
 */

/**
 * Function to generate JSON for a random recipe.
 *
 * @return string
 *  JSON
 */
function cookbook_random_recipe_xhr() {
  print drupal_to_js(cookbook_get_random_recipe());
}

/**
 * Prints a JSON containing a list with all available ingredients.
 *
 * @return string
 *  JSON.
 */
function cookbook_ingredients_xhr() {
  $query = "SELECT n.nid, n.title, GROUP_CONCAT(cu.id SEPARATOR ';') as unit_ids, GROUP_CONCAT(cu.name SEPARATOR ';') as units
    FROM {node} n
    LEFT JOIN {cookbook_units} cu ON n.vid = cu.ingredient_vid
    WHERE n.type = 'ingredient' AND n.status = 1
    ";
  $query_args = array();

  // Check language
  if (module_exists('i18ntaxonomy')) {
    $query .= " AND n.language IN('','%s')";
    $query_args[] = i18n_get_lang();
  }

  $result = db_query($query . "GROUP BY n.vid ORDER BY n.title", $query_args);

  $ingredients = array();
  while ($ingredient = db_fetch_object($result)) {
    $ingredient->unit_ids = explode(';', $ingredient->unit_ids . ';0');
    $ingredient->units = explode(';', $ingredient->units . ';g');
    $ingredients[] = $ingredient;
  }

  print drupal_to_js($ingredients);
}

/**
 * Search for a recipe by name.
 *
 * @param string $search Start of word.
 */
function cookbook_partial_xhr($search) {
  $query = "SELECT nid, title FROM {node} WHERE type = 'recipe' AND status = 1 AND title LIKE '%s%%'";
  $query_args = array($search);

  // Check language
  if (module_exists('i18ntaxonomy')) {
    $query .= " AND language IN('','%s')";
    $query_args[] = i18n_get_lang();
  }

  $result = db_query($query, $query_args);

  $recipes = array();
  while ($recipe = db_fetch_object($result)) {
    $recipes[$recipe->nid . ' (' . $recipe->title . ')'] = $recipe->title;
  }
  drupal_json($recipes);
}

/**
 * Print a JSON with all the categories for the commodities.
 */
function cookbook_commodities_categories_xhr() {
  if (module_exists('i18ntaxonomy')) {
    $commodities = cookbook_get_localized_tree(variable_get('cookbook_commodities', NULL));
  }
  else {
    $commodities = taxonomy_get_tree(variable_get('cookbook_commodities', NULL));
  }
  print drupal_to_js($commodities);
}

/**
 * Print a JSON with all the unique first letters of the terms from the
 * dictionary vocabulary.
 */
function cookbook_dictionary_letters_xhr() {
  // Begin the query
  $query = "SELECT DISTINCT UCASE(LEFT(name, 1)) AS first_letter FROM {term_data} WHERE vid = %d";
  $query_args = array(variable_get('cookbook_dictionary', NULL));

  // Only select the terms that are translated
  if (module_exists('i18ntaxonomy')) {
    $query .= " AND language = '%s'";
    $query_args[] = i18n_get_lang();
  }

  // Order
  $query .= " ORDER BY name";

  // Do the query
  $result = db_query($query, $query_args);

  // Process the result
  $letters = array();
  while ($term = db_fetch_object($result)) {
    $letters[] = $term->first_letter;
  }

  // Print the JSON
  print drupal_to_js($letters);
}

/**
 * Print a JSON with all the terms that beings with the given letter.
 *
 * @param char $letter
 *   The first letter of the terms, @ for all terms.
 */
function cookbook_dictionary_letter_xhr($letter) {
  $letter = urldecode($letter);

  // Begin building the query
  $query = "SELECT tid, name, description FROM {term_data} WHERE vid = %d";
  $query_args = array(variable_get('cookbook_dictionary', NULL));

  // Filter on the right letter
  if ($letter != '@') {
    $query .= " AND LEFT(name, 1) = '%s'";
    $query_args[] = $letter;
  }

  // Only select the terms that are translated
  if (module_exists('i18ntaxonomy')) {
    $query .= " AND language = '%s'";
    $query_args[] = i18n_get_lang();
  }

  // Order
  $query .= " ORDER BY name";

  // Do the query
  $result = db_query($query, $query_args);

  // Process the result
  $words = array();
  while ($term = db_fetch_object($result)) {
    $words[] = $term;
  }

  // Print the JSON
  print drupal_to_js($words);
}

/**
 * Print a JSON with all the commodities that belongs to the category.
 *
 * @param int $category
 *   The category identifier, or term id if you will. @ for all commodities.
 */
function cookbook_commodities_category_xhr($category) {
  global $language; 
  // Begin building the query
  $query = "SELECT nr.vid, nr.nid, nr.title, nr.teaser FROM {node} n
      INNER JOIN {node_revisions} nr ON nr.vid = n.vid
      INNER JOIN {content_type_fagstoff} ctf ON ctf.vid = n.vid
      WHERE n.status=1 AND n.language IN ('','%s')";
  $query_args = array($language->language);

  // Add filtering
  if ($category == '@') {
    // Fetch all commodities from all categories
    $categories = array();
    foreach (taxonomy_get_tree(variable_get('cookbook_commodities', NULL)) as $category) {
      $categories[] = $category->tid;
    }

    $query .= ' AND (ctf.field_cookbook_commodities_value IN(%s))';
    $query_args[] = implode(',', $categories);
  }
  else {
    // Fetch only for given category
    $query .= ' AND (ctf.field_cookbook_commodities_value = %d)';
    $query_args[] = $category;
  }

  // Order the commodities
  $query .= ' ORDER BY nr.title';
  //echo $query . "<br />";
  //echo "<pre>" . print_r($query_args, true) . "</pre>";
  //die("ok");
  // Execute the query
  $result = db_query($query, $query_args);

  // Process the result
  $commodities = array();
  while ($commodity = db_fetch_array($result)) {
    // Strip all tags and brackets
    $commodity['teaser'] = preg_replace('/\[.*\]/', '', strip_tags($commodity['teaser']));
    // Add url
    $commodity['url'] = url('node/' . $commodity['nid']);
    $commodities[] = $commodity;
  }
  
  // Allow other modules to alter the commodities.
  $commodities_altered = module_invoke_all('commodities_category_xhr_alter', $commodities);

  // Print as a JSON
  print drupal_to_js($commodities_altered ? $commodities_altered : $commodities);
}

/**
 * Prints a JSON with all the values for the converter.
 */
function cookbook_converter_xhr() {
  print drupal_to_js(
      array(
        t('volume') => array(
          array('name' => t('ml (millilitre)'), 'ratio' => 0.001),
          array('name' => t('cl (centilitre)'), 'ratio' => 0.01),
          array('name' => t('dl (decilitre)'), 'ratio' => 0.1),
          array('name' => t('l (litre)'), 'ratio' => 1),
          array('name' => t('spm (spice measure)'), 'ratio' => 0.001),
          array('name' => t('ts (teaspoon)'), 'ratio' => 0.005),
          array('name' => t('tb (tablespoon)'), 'ratio' => 0.015),
          array('name' => t('cup'), 'ratio' => 0.25),
          array('name' => t('coffeecup'), 'ratio' => 0.15),
          array('name' => t('grams wheat flour'), 'ratio' => 0.002),
          array('name' => t('grams coarse flour'), 'ratio' => 0.0018),
          array('name' => t('grams flour'), 'ratio' => 0.0014),
          array('name' => t('grams potato flour'), 'ratio' => 0.00125),
          array('name' => t('grams oatmeal'), 'ratio' => 0.00286),
          array('name' => t('grams baking powder'), 'ratio' => 0.00143),
          array('name' => t('grams salt'), 'ratio' => 0.00067),
          array('name' => t('grams sugar'), 'ratio' => 0.00118),
          array('name' => t('grams powdered sugar'), 'ratio' => 0.00167),
          array('name' => t('grams spice'), 'ratio' => 0.0014),
          array('name' => t('grams nut kernels'), 'ratio' => 0.002),
          array('name' => t('grams syrup'), 'ratio' => 0.00067),
          array('name' => t('grams raisins'), 'ratio' => 0.00167),
          array('name' => t('grams butter'), 'ratio' => 0.0006),
          array('name' => t('grams melted butter'), 'ratio' => 0.001),
          array('name' => t('grams oil'), 'ratio' => 0.00118),
          array('name' => t('grams semolina'), 'ratio' => 0.00148),
          array('name' => t('grams rice meal'), 'ratio' => 0.00105),
          array('name' => t('grams peas'), 'ratio' => 0.00105),
          array('name' => t('grams brown beans'), 'ratio' => 0.001),
          array('name' => t('grams powdered rusk'), 'ratio' => 0.0022),
          array('name' => t('grams dried haws'), 'ratio' => 0.00235),
          array('name' => t('grams almonds'), 'ratio' => 0.00154),
          array('name' => t('grams chopped onion'), 'ratio' => 0.002),
          array('name' => t('grams raw potato'), 'ratio' => 0.00143),
          array('name' => t('grams boiled potato'), 'ratio' => 0.00154),
          array('name' => t('grams boiled apple'), 'ratio' => 0.002),
          array('name' => t('grams berries'), 'ratio' => 0.00182),
          array('name' => t('dal (decaliter)'), 'ratio' => 10.0),
          array('name' => t('hl (hectoliter)'), 'ratio' => 100.0),
          array('name' => t('kl (kiloliter)'), 'ratio' => 1000.0),
          array('name' => t('teaspoon (US)'), 'ratio' => 0.004929),
          array('name' => t('tablespoon (US)'), 'ratio' => 0.01479),
          array('name' => t('fluid ounce (US)'), 'ratio' => 0.02957),
          array('name' => t('gill (US)'), 'ratio' => 0.118),
          array('name' => t('cup (US)'), 'ratio' => 0.2366),
          array('name' => t('pint (US)'), 'ratio' => 0.4732),
          array('name' => t('quart (US)'), 'ratio' => 0.9463),
          array('name' => t('gallon (US)'), 'ratio' => 3.785),
          array('name' => t('cubic inch'), 'ratio' => 0.016387),
          array('name' => t('teaspoon (UK)'), 'ratio' => 0.00616),
          array('name' => t('dessert spoon (UK)'), 'ratio' => 0.01232),
          array('name' => t('tablespoon (UK)'), 'ratio' => 0.01848),
          array('name' => t('fluid ounce (UK)'), 'ratio' => 0.0284),
          array('name' => t('gill (UK)'), 'ratio' => 0.142),
          array('name' => t('breakfast cup (UK)'), 'ratio' => 0.284),
          array('name' => t('pint (UK)'), 'ratio' => 0.568),
          array('name' => t('quart (UK)'), 'ratio' => 1.136),
          array('name' => t('gallon (UK)'), 'ratio' => 4.546)
        ),
        t('weight') => array(
          array('name' => t('kg (kilograms)'), 'ratio' => 1000.0),
          array('name' => t('hg (hectograms)'), 'ratio' => 100.0),
          array('name' => t('dag (decagrams)'), 'ratio' => 10.0),
          array('name' => t('g (grams)'), 'ratio' => 1.0),
          array('name' => t('dg (decigrams)'), 'ratio' => 0.1),
          array('name' => t('cg (centigrams)'), 'ratio' => 0.01),
          array('name' => t('mg (milligrams)'), 'ratio' => 0.001),
          array('name' => t('litre wheat flour'), 'ratio' => 500.0),
          array('name' => t('litre coarse flour'), 'ratio' => 550.0),
          array('name' => t('litre flour'), 'ratio' => 733.33),
          array('name' => t('litre potato flour'), 'ratio' => 800.0),
          array('name' => t('litre oatmeal'), 'ratio' => 350.0),
          array('name' => t('litre baking powder'), 'ratio' => 700.0),
          array('name' => t('litre salt'), 'ratio' => 1500.0),
          array('name' => t('litre sugar'), 'ratio' => 850.0),
          array('name' => t('litre powdered sugar'), 'ratio' => 600.0),
          array('name' => t('litre spice'), 'ratio' => 700.0),
          array('name' => t('litre nut kernels'), 'ratio' => 500.0),
          array('name' => t('litre syrup'), 'ratio' => 1500.0),
          array('name' => t('litre raisins'), 'ratio' => 600.0),
          array('name' => t('litre butter'), 'ratio' => 1666.67),
          array('name' => t('litre melted butter'), 'ratio' => 1000.0),
          array('name' => t('litre oil'), 'ratio' => 850.0),
          array('name' => t('litre semolina'), 'ratio' => 675.0),
          array('name' => t('litre rice meal'), 'ratio' => 950.0),
          array('name' => t('litre peas'), 'ratio' => 950.0),
          array('name' => t('litre brown beans'), 'ratio' => 1000.0),
          array('name' => t('litre powdered rusk'), 'ratio' => 450.0),
          array('name' => t('litre dried haws'), 'ratio' => 425.0),
          array('name' => t('litre almonds'), 'ratio' => 650.0),
          array('name' => t('litre chopped onion'), 'ratio' => 500.0),
          array('name' => t('litre raw potato'), 'ratio' => 700.0),
          array('name' => t('litre boiled potato'), 'ratio' => 650.0),
          array('name' => t('litre boiled apple'), 'ratio' => 500.0),
          array('name' => t('litre berries'), 'ratio' => 550.0),
          array('name' => t('gr (grain)'), 'ratio' => 0.0648),
          array('name' => t('oz (ounces)'), 'ratio' => 28.34952),
          array('name' => t('Ib (pound)'), 'ratio' => 453.592),
          array('name' => t('st (stone)'), 'ratio' => 6350),
          array('name' => t('qu (quarter)'), 'ratio' => 12700)
        ),
        t('degrees') => array(
          array('name' => t('celcius'), 'ratio' => -1.0),
          array('name' => t('fahrenheit'), 'ratio' => -1.0)
        ),
        t('amount') => array(
          array('name' => t('pieces'), 'ratio' => 1.0),
          array('name' => t('dozen'), 'ratio' => 12.0),
          array('name' => t('score'), 'ratio' => 20.0),
          array('name' => t('gross'), 'ratio' => 144.0),
          array('name' => t('great gross'), 'ratio' => 1728.0),
        )
      )
  );
}