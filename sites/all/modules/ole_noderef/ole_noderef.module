<?php
/**
 * 	@file
 * 	Adds a browse button to each (cck) nodereferance autocomplete form item rendered in the node edit form
 *
 * 	@author Ole Mahtte Scheie Anti
 *
 * 	@version 6.1
 *  
 * 	@todo
 *  The browser button breaks if you push the add more button.
 * 	@warning
 *
 *	@license
 *	Copyright (C) Utdanning.no
 *
 * 	 This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the LISENCE.txt file or the
 *   GNU General Public License for more details.
 * 
 */

/*
 * Implementation of hook_menu()
 */
function ole_noderef_menu(){
	$items = array();
  $items['ole_noderef'] = array(
    'title' => t('Browse'),
    'type' => MENU_CALLBACK,
    'page callback' => 'ole_noderef',
    'access arguments' => array('access content'),
  );
  $items['ole_noderef_old'] = array(
    'title' => t('Browse'),
    'type' => MENU_CALLBACK,
    'page callback' => 'ole_noderef',
    'access arguments' => array('access content'),
  );
  
  return $items;
}

/*
 * Implementation of hook_form_alter()
 */
function ole_noderef_form_alter (&$form, $form_state, $form_id){									   
	// checks if form is a node_form
	if(substr($form_id, strlen($form_id)-9)=='node_form'){
		$all_fields=array(); //contains form fieldnames to all nodereference autocomplete fields
	
		// finds all nodereference fields
		if($form["#field_info"]){
			foreach ($form["#field_info"] as $field_name => $field){
				if($field["type"]=="nodereference" && $field["widget"]["type"]=="nodereference_autocomplete"){
					$all_fields[]=$field_name;
				}
			}
		}

		drupal_add_css(drupal_get_path("module","ole_noderef")."/ole_noderef.css");

		drupal_add_js(drupal_get_path("module","ole_noderef")."/java.js");
		drupal_add_js('var modulepath="'.drupal_get_path('module', 'ole_noderef').'";','inline');

		// ends form_alter if no nodereference fields are present
		if(count($all_fields)<1){
			return false;
		}

		// prints a js call foreach nodereference field to make_browser_buttons(), wich uses jquery to make the browser link
		foreach ($all_fields as $f){
			$js.="make_browser_buttons('".str_replace('_','-',$f)."'); ";
			//drupal_add_js("Drupal.behaviors.ole_noderef = function (context) { make_browser_buttons('".str_replace('_','-',$f)."');};", 'inline');				
		}	
		drupal_add_js("Drupal.behaviors.ole_noderef = function (context) { ". $js ."};","inline");

	} else {
    $field_name = '';
		foreach($form as $f => $value) {
			$field_name = $f;
		}
    /*
    if(isset($form[$field_name][0]['#type']) && $form[$field_name][0]['#type'] == 'nodereference_autocomplete') {
      $form[$field_name][$field_name.'_add_more']['#attributes'] = array("onsubmit" => "alert('ok');");
    }*/
	}
	
	if($form_id=="utdanning_mmenu_links_add_or_edit_link"){
		$js="

		
		var link='<a href=\"'+Drupal.settings.basePath+modulepath+'/browse.php\" rel=\"lightmodal\>'+Drupal.t('Browse')+'</a>';
		$('#edit-link-wrapper').append(link);

		
	$('#edit-link-wrapper a, #edit-link-wrapper a').click(function(e){		
		clickedid=$(this).parent().attr('id');
		
		$('#lightbox').unbind('click');
		Lightbox.start(this, false, false, false, true);
		if (e.preventDefault) { e.preventDefault(); }
		return false;
	});
		
		
		";
		drupal_add_css(drupal_get_path("module","ole_noderef")."/ole_noderef.css");
		drupal_add_js('var modulepath="'.drupal_get_path('module', 'ole_noderef').'";','inline');
		drupal_add_js(drupal_get_path("module","ole_noderef")."/java.js");
		drupal_add_js("Drupal.behaviors.ole_noderef = function (context) {".$js."};", "inline");
	}
}

/**
 * Finds all content types a nodereference field can refeer to
 *
 * @param string $field
 * @return array $ret
 */
function ole_noderef_get_types($field) {
  $ret = array();
  
  $types = node_get_types(); // all content types on the site
  foreach($field['referenceable_types'] as $type => $enabled) {
    if($enabled) {
      $ret[$type] = $types[$type]->name;
    }
  }

  return $ret;
}

/**
 * Gets contenttypes the utdanning_rdf module can relate to
 *
 * @param string $f
 * @return array
 */
function ole_noderef_get_types_rdf($f){
	$t = array_keys(utdannig_rdf_from_ct_to_cts($f));
	foreach ($t as $type){
		$out[$type]=$type;
	}
	return $out;
}
/**
 * This is the "main" function of the module. It is called by the page callback and returns nodes to the browser in json.
 *
 * @param string $f fieldname of the nodereference field
 * @return 
 */
function ole_noderef($f){
  	global $language;
  	//ole_noderef/field_type/string?page=0&hits=10&user=0&tax=0&sort=changed&sortby=desc
  	$_GET["page"]=$_GET["page"] ? ($_GET["page"]-1):0; // current page in the browser
  	$hits=$_GET["hits"] ? $_GET['hits'] : 10; // number of nodes pr page
	$user=$_GET["page"] ? $_GET['page'] : 0;
	$tax=$_GET["tax"]? $_GET['tax'] : '0'; // taxonomy terms to search in
	$sort=$_GET["sort"]? $_GET['sort'] : 'changed'; // field to sort the result in
	$sortby=$_GET["sortby"] ? $_GET['sortby'] : 'desc'; // sortby desc/asec
	$type_c=$_GET["type"] ? $_GET['type'] : 'all'; // node types to search in
	$lang=$_GET["lang"] ? $_GET['lang'] : ''; // node language to search in

	if($lang=="all"){ $lang="null"; }

	$related_types = array();
  	$where = array();
  	$args = array();
	$content_fields=content_fields();
	$arguments = func_get_args();
  	$string = $arguments[1]; // this is the search string in the browser
	
  	// Allowed sorting
	global $order_by;
  	$orderby["changed"]="changed";
  	$orderby["title"]="title";
  	$orderby["user"]="user";
  	$orderby_order["desc"]="desc";
  	$orderby_order["asc"]="asc";
  	$order_by=$orderby[$sort]." ".$orderby_order[$sortby];
  	
  	if($_GET["from"]=="rdf"){  
		  $field['referenceable_types'] = ole_noderef_get_types_rdf($f);
		  $searchable_types = $field['referenceable_types'];
		  $field['advanced_view'] = variable_get('utdanning_rdf_view_name', 'noderef');
  	}elseif($arguments[0]=="mmenu"){
  		 $field['referenceable_types']["fagstoff"]="fagstoff";
  		 $field['referenceable_types']["image"]="Bilde";
  		 $field['referenceable_types']["begrep"]="Begrep";
  		 $field['referenceable_types']["audio"]="Lyd";
  		 $field['referenceable_types']["oppgave"]="Oppgave";
  		 $field['referenceable_types']["person"]="Person";
  		 $field['referenceable_types']["quiz"]="quiz";
  		 $field['referenceable_types']["test"]="test";
  		 $field['referenceable_types']["veiledning"]="Veiledning";
  		 $field['advanced_view']=NULL;
  		 $field['advanced_view_args']=NULL;
  		 $searchable_types=$field['referenceable_types'];
  	}else{
  		
		$field=$content_fields[$f]; // the nodereference field the browser is called from
  		$searchable_types = ole_noderef_get_types($field);
  	}
 	
  	//$searchable_types["story"]="story";
	$match = isset($field['widget']['autocomplete_match']) ? $field['widget']['autocomplete_match'] : 'contains';	
	
	/*
	 * A nodereference field can use a view to define refeerable nodes, or use checkboxes to manually choose node types.
	 * The views part of this module is based on the nodereference.module code. It makes a new display to the view.
	 */

	// VIEWS PART
  	if (module_exists('views') && !empty($field['advanced_view']) && $field['advanced_view'] != '--') {
  		
    	$view_name=$field['advanced_view'];
    	if ($view = views_get_view($view_name)) {   // We add a display, and let it derive from the 'default' display.
      		// TODO: We should let the user pick a display in the fields settings - sort of requires AHAH...
      		$display = $view->add_display('content_references');
      		$view->set_display($display);

	      	// TODO from merlinofchaos on IRC : arguments using summary view can defeat the style setting.
	      	// We might also need to check if there's an argument, and set *its* style_plugin as well.
	      	$view->display_handler->set_option('style_plugin', 'content_php_array_autocomplete');
	      	$view->display_handler->set_option('row_plugin', 'fields');
	      	// Used in content_plugin_style_php_array::render(), to get
	      	// the 'field' to be used as title.
	      	$view->display_handler->set_option('content_title_field', 'title');
	
	      	// Additional options to let content_plugin_display_references::query()
	      	// narrow the results.

      		$options = array(
		        'table' => 'node',
		        'field_string' => 'title',
		        'string' => $string,
		        'match' => $match,
		        'field_id' => 'nid',
		        'ids' => $ids,
      		);

      		$view->display_handler->set_option('content_options', $options);

	      	// TODO : for consistency, a fair amount of what's below
		    // should be moved to content_plugin_display_references
		
		    // Limit result set size.
    
      		$view->display_handler->set_option('items_per_page', $hits);
      		$view->display_handler->set_option('use_pager', TRUE);
  		    $view->display_handler->set_option('current_page', $_GET["page"]);
      		$view->display_handler->set_option('element', $_GET["page"]);
      		$view->set_use_pager(TRUE);
      		$view->pager["element"]=3;
      		$view->pager["items_per_page"]=$hits;
      		$view->pager["current_page"]=$_GET["page"];

      		// Get arguments for the view.
      		if (!empty($field['advanced_view_args'])) {
        	// TODO: Support Tokens using token.module ?
        		$view_args = array_map('trim', explode(',', $field['advanced_view_args']));
      		} else {
        		$view_args = array();
      		}

      		// We do need title field, so add it if not present (unlikely, but...)
      		$fields = $view->get_items('field', $display);
      		if (!isset($fields['title'])) {
        		//$view->add_item($display, 'field', 'node', 'title');
      		}
	
      		// If not set, make all fields inline and define a separator.
      		$options = $view->display_handler->get_option('row_options');
      		if (empty($options['inline'])) {
        		$options['inline'] = drupal_map_assoc(array_keys($view->get_items('field', $display)));
      		}

      		if (empty($options['separator'])) {
        		$options['separator'] = '-';
      		}
    
      		$view->display_handler->set_option('row_options', $options);

      		// Make sure the query is not cached
      		$view->is_cacheable = FALSE;
	
      		// SORTING
    
	      	$orderby[$sort]." ".$orderby_order[$sortby];
	      	if($orderby[$sort]=="title"){
	    	  	$view->add_item($display, "sort", "node", "title", array("order"=> $orderby_order[$sortby], "relationship" => "none"), "title");
	      	}
	      	if($orderby[$sort]=="changed"){
	    	  	$view->add_item($display, "sort", "node", "changed", array("order" => $orderby_order[$sortby], "granularity" => "second", "relationship" => "none"),"changed");
	      	}
	      	if($orderby[$sort]=="user"){
	    	  	$view->add_item($display, "sort", "users", "uid", array("order"=> $orderby_order[$sortby], "relationship" => "none"),"uid");
	      	}
    
      		// FILTER
      		if($type_c != "all" && !empty($type_c)){
    	  		$view->add_item($display, "filter", "node", "type", array("operator"=>"in", "value" =>array($type_c=>$type_c), "group"=>0, "exposed" => "", "expose"=>array("operator" =>"", "label"=>""),"relationship"=>"none"),"type");
      		} else {
        		$view->add_item($display, "filter", "node", "type", array("operator"=>"in", "value" => array_keys($searchable_types), "group"=>0, "exposed" => "", "expose"=>array("operator" =>"", "label"=>""),"relationship"=>"none"),"type");
      		}
    
      		if($tax>0){
        		$view->add_item($display, "filter", "term_data", "vid", array("operator"=>"in", "value" =>array(""=>""), "group"=>0, "exposed" => "", "expose"=>array("operator" =>"", "label"=>""),"relationship"=>"none"),"vid");
      		}
  
			if ($lang && $lang != 'null' && !empty($lang) && $lang != 'all'){
				$view->add_item($display, "filter", "node", "language", array("operator"=>"in", "value" =>array($lang =>$lang), "group"=>0, "exposed" => "", "expose"=>array("operator" =>"", "label"=>""),"relationship"=>"none"),"language");
      		}

      		// Get the results.

      		$view->execute($display);
      		$result = $view->render($display);

      		// GENERATE JSON FROM NODES
      		foreach ($result as $nid => $data) {
      			$node=node_load(array("nid"=>$nid));     	
  		  		if($node->type=='image'){  			
  		  			$i++;
		          	$author=user_load(array('uid' => $node->uid));
		          	$references[] = array(
				        'nid' => $node->nid,
				        'title' => $node->title,   
				        'language' => $node->language,
				        'type' => $node->type,
				        'user' => $author->name,
				        'updated' => date("d.m.Y",$node->changed),
			    	    'image' => $node->images['thumbnail'],
			    	);
  		  		} else {
  			  		$i++;
  	  				$author=user_load(array('uid' => $node->uid));
	       			$references[] = array(
				       	'nid' => $node->nid,
				       	'title' => $node->title,
				       	'language' => $node->language,
				       	'type' => $node->type,
				       	'user' => $author->name,
				       	'updated' => date("d.m.Y",$node->changed),
	        		);
        		}
      		}
    
      $json["nodes"]=$references;
      $json["total"]=$view->total_rows; // number of nodes
      $json["pages"]=ceil($view->total_rows / $view->pager["items_per_page"]); // number of pages
      $json["page"]=$_GET["page"]+1; // current page
      $json["sort"]=$orderby[$sort]; // sorting by
      $json["sort_type"]=$orderby_order[$sortby]; 
      $json["type_c"]=$type_c;
      $json["hits"]=$hits; 
      $json['type'] = $searchable_types;

      //Get the sites languages
      $languages = array();
      $languages['all'] = t('Language');
      foreach(language_list() as $enabled_lang) {
        $languages[$enabled_lang->language] = $enabled_lang->native;
      }
      $json['languages'] = $languages;
      $json['selected_language'] = check_plain($lang);
      drupal_json($json);
    
      return;
    }
  }
	
	// STANDARD
	
	if(!$result){
		//type stuff
	    if(!empty($lang)) {
	    }
	    
	    if (is_array($field['referenceable_types'])) {
	      foreach (array_filter($field['referenceable_types']) as $related_type) {
	        if($type_c=="all"){	    		
	          $related_types[] = "n.type = '%s'";
	          $args[] = $related_type;	      		
	        }
	        else if($type_c==$related_type) {
	          $related_types[] = "n.type = '%s'";
	          $args[] = $related_type;				
	        }
	      }
	    }
	    
	    $where[] = implode(' OR ', $related_types);	
		
	    if (!count($related_types)) {
	      return array();
	     }
		
	    if ($string !== '' ) {
	      $match_operators = array(
	        'contains' => "LIKE '%%%s%%'",
	        'equals' => "= '%s'",
	        'starts_with' => "LIKE '%s%%'",
	      );
	
	      $where[] = 'n.title '. (isset($match_operators[$match]) ? $match_operators[$match] : $match_operators['contains']);
	      $args[] = $string;
	    }
		  	
	    $where_clause = $where ? 'WHERE ('. implode(') AND (', $where) .')' : '';
	    $sql = db_rewrite_sql("SELECT n.nid, n.changed, n.title AS node_title, n.type AS node_type, n.uid, n.language FROM {node} n $where_clause ORDER BY $order_by");
	      
	    /* db_rewrite_sql adds language as a filter, remove it */
	    if(empty($lang)) {
	      $sql = str_replace("(n.language ='".$language->language."') AND", "", $sql);
	    }
	    else {
	      $sql = str_replace("n.language ='".$language->language."'", "n.language = '$lang'", $sql);
	    }
			//$result=db_query($sql,$args);
	    $result=pager_query($sql, $hits, $element = 0, $count_query = NULL, $args);	
	  }
	
	  $references = array();
	  $i=0;
	  
	  // go thru all the nodes from the result
	  while ($node = db_fetch_object($result)) {
	  	// if result node is a image, also add the thumb to the result
	    if($node->node_type=='image'){
	      $node_node=node_load(array('nid'=>$node->nid));
	      $i++;
	      $author=user_load(array('uid' => $node->uid));
	      $references[] = array(
	        'nid' => $node->nid,
	        'title' => $node->node_title,   
	        'type' => $node->node_type,
	        'user' => $author->name,
	        'language' => $node->language,
	        'updated' => date("d.m.Y",$node->changed),
	        'image' => $node_node->images['thumbnail'],	      		
	      );  
	    }else{
	    	
	      $i++;
	      $author=user_load(array('uid' => $node->uid));
	      $references[] = array(
	        'nid' => $node->nid,
	        'title' => $node->node_title,   
	        'type' => $node->node_type,
	        'user' => $author->name,
	        'language' => $node->language,
	        'updated' => date("d.m.Y",$node->changed),
	      );
	    }
	  }
	  
	  global $pager_page_array, $pager_total, $pager_total_items;
	  $json["nodes"]=$references;
	  $json["total"]=$pager_total_items[0];
	  $json["pages"]=$pager_total[0];
	  $json["page"]=$_GET["page"]+1;
	  $json["sort"]=$orderby[$sort];
	  $json["sort_type"]=$orderby_order[$sortby];
	  $json["type_c"]=$type_c;
	  $json["hits"]=$hits;
	  $json["type"] = $searchable_types;
	  //Get the sites languages
      $languages = array();
      $languages['all'] = t('Language');
      foreach(language_list() as $enabled_lang) {
        $languages[$enabled_lang->language] = $enabled_lang->native;
      }
      $json['languages'] = $languages;
      $json['selected_language'] = check_plain($lang);

	  drupal_json($json);
}

