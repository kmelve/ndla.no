<?php 
  $vector = array();
  if(!empty($parents)) {
    foreach($parents as $parent) {
      if(empty($parent->type_name)) {
        $vector[$parent->type][] =  l($parent->title,$parent->url);
      } else {
        $classes = '';
        if($parent->current) $classes .= ' current';
        $key = $parent->type_name . ', ' . l($parent->course_name,$parent->course_url);
        $vector[$key][] =  "<a href='".$parent->url."' class='$classes'>".$parent->title.'</a>';
      }
    }

    foreach($vector as $headline => $items) {
      echo '<h3 class="partof-parent-headline">' . $headline . '</h3>';
      foreach($items as $item) {
        echo '<span class="partof-item">' . $item . '</span>';
      }  
    }
  }
