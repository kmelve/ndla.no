    <ul>
    <?php
    foreach($courses as $course) {
      $span = '';
      if(!empty($course->in_dev)) {
        $span = "<span class='beta'><i class='icon-exclamation-sign'></i>Beta</span>";
      }
      print "<li class='$class'>";
      print "<a class='subject_link' href='" . $course->url ."'>" .  trim($course->name) . $span . "</a>";
      print "</li>";
    }
    ?>
    </ul>
