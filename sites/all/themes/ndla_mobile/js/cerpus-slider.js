Drupal.behaviors.ndla_mobile = function() {
  $('.slider a.show').bind('click', function() {
    $('.content', $(this).parents("div.slider:eq(0)")).slideDown();
     $('.content a.hide', $(this).parents("div.slider:eq(0)")).show();
     $(this).hide();
  });
  $('.slider a.hide').bind('click', function() {
    $('.content', $(this).parents("div.slider:eq(0)")).slideUp();
    $('.content a.show', $(this).parents("div.slider:eq(0)")).show();
    $(this).hide();
  });
}

var flip_init = false;
Drupal.behaviors.ndla_flipper  = function() {

(function($) {
  
  if(flip_init) {
    return;
  }
  flip_init = true;
  
  var selector = '.flip-clicker';

  if(Modernizr.touch) {
    $('body').on('touchstart', selector, flip_function);
  } else {
    $(selector).click(flip_function);
  }

  function flip_function(ev) {
      var max_height = 0;
      var page1 = $(this).parents('.paragraph.container').children('.frontside');
      var page2 = $(this).parents('.paragraph.container').children('.backside');

      var toHide = page1.is(':visible') ? page1 : page2;
      var toShow = page2.is(':visible') ? page1 : page2;
      
      toHide.removeClass('flip in').addClass('flip out').hide();
      toShow.removeClass('flip out').addClass('flip in').show();
  }
}(jq17));
}