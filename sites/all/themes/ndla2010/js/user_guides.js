Drupal.behaviors.userGuides = function($context) {
  $('.user-guide:not(.user-guides-processed)')
  .addClass('user-guides-processed')
  .click(UserGuides.clicked)
  .hover(UserGuides.rolledOver, UserGuides.rolledOut);
  $('#user-guide-close:not(.user-guides-processed)')
  .addClass('user-guides-processed')
  .click(UserGuides.closeClicked);
  if (!UserGuides.inited) {
    UserGuides.originalThumbnail = $('#user-guide-thumbnail').attr('src');
  }
  UserGuides.inited = true;
};

var UserGuides = UserGuides || {};

UserGuides.fullScreen = false;
UserGuides.inited = false;

UserGuides.rolledOver = function(event) {
  $('#user-guide-thumbnail').attr('src', Drupal.settings['user_guide_thumbnails'][$(this).attr('href').split('?')[0].match(/\d+$/)]);
  $('#user-guide-thumbnail-text').hide();
};

UserGuides.rolledOut = function(event) {
  $('#user-guide-thumbnail').attr('src', UserGuides.originalThumbnail);
  $('#user-guide-thumbnail-text').show();
};

UserGuides.clicked = function(event) {
  event.preventDefault();
  var $loader = $('#user-guide-movie');
  $loader.parent().show();
  UserGuides.loadLibrary();
  UserGuides.loadCSS();
  $loader.html('<img id="user-guide-loader" src="' + Drupal.settings['ndla_utils_disp']['themeUrl'] + '/img/throbber.gif" alt="' + Drupal.t('Loading') + '"/>');
  $.ajax({
    type: "GET",
    url: $(this).attr('href'),
    data: {ajax: 1},
    dataType: "html",
    success: function(html) {
      $loader.html(html);
      VideoJS.setup("All", {
        controlsBelow: false, // Display control bar below video instead of in front of
        controlsHiding: true, // Hide controls when mouse is not over the video
        defaultVolume: 0.85, // Will be overridden by user\'s last volume if available
        flashVersion: 9, // Required flash version for fallback
        linksHiding: true // Hide download links when video is supported});
      });
      $('.vjs-fullscreen-control').click(UserGuides.fullscreenClicked);
    }
  });
};

UserGuides.fullscreenClicked = function(event) {
  UserGuides.fullScreen = !UserGuides.fullScreen;
  if (!UserGuides.fullScreen) {
    $('#user-guide-box').addClass('on-top').addClass('tray-stand-out');
    $('html, body').animate({scrollTop: $(document).height()}, 1);
  }
  else {
    $('#user-guide-box').removeClass('on-top').removeClass('tray-stand-out');
  }
};

UserGuides.closeClicked = function(event) {
  $('#user-guide-movie').html('');
  $('#user-guide-box').hide();
};

// css and js are only loaded when they are needed.
// Most users will not need these because they don't need
// user guides.
UserGuides.loadLibrary = function() {
  if (UserGuides.libraryLoaded) {
    return;
  }
  UserGuides.libraryLoaded = true;
  $.ajax({
    type: "GET",
    url: Drupal.settings.ndla_utils.htmlvideoLibraryUrl,
    dataType: "script",
    success: function(script) {
      eval(script);
    }
  });
};

UserGuides.loadCSS = function() {
  if (UserGuides.cssLoaded) {
    return;
  }
  UserGuides.cssLoaded = true;
  var fileref = document.createElement("link")
  fileref.setAttribute("rel", "stylesheet")
  fileref.setAttribute("type", "text/css")
  fileref.setAttribute("href", Drupal.settings.ndla_utils.htmlvideoCSSUrl)
  document.getElementsByTagName('head')[0].appendChild(fileref);
};