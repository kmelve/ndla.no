<?php

$types = node_get_types();

$counter = 1;
print '<div class="contentbrowser-rel-node-list">';
foreach($nids as $nid_object) {
  if(is_numeric($nid_object)) {
    $tmp = $nid_object;
    $nid_object = new stdClass();
    $nid_object->nid = $tmp;
  }

  print "<div class='contentbrowser-rel-node'>";
  $node = node_load($nid_object->nid);
  print '<div class="contentbrowser-node-title">' . l($node->title, "node/".$node->nid)." (".$types[$node->type]->name. ")</div>";
  
  print theme('ndla_authors_block_view', $node);

  $counter++;
  print '</div>';
}
print '</div>';
?>
